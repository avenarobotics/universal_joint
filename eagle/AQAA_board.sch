<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="144" name="DrillLegend" color="7" fill="1" visible="no" active="yes"/>
<layer number="145" name="DrillLegend_01-16" color="2" fill="9" visible="no" active="yes"/>
<layer number="146" name="DrillLegend_01-20" color="3" fill="9" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="AQAA">
<packages>
<package name="DIODFN420X360X332-3N" urn="urn:adsk.eagle:footprint:31059761/1" locally_modified="yes">
<description>DFN3, 4.20 X 3.60 X 3.33 mm body
&lt;p&gt;DFN3 package with body size 4.20 X 3.60 X 3.33 mm&lt;/p&gt;</description>
<circle x="-2.604" y="0.005" radius="0.25" width="0" layer="21"/>
<wire x1="2.1" y1="1.8" x2="-2.1" y2="1.8" width="0.12" layer="21"/>
<wire x1="2.1" y1="-1.8" x2="-2.1" y2="-1.8" width="0.12" layer="21"/>
<wire x1="2.1" y1="-1.8" x2="-2.1" y2="-1.8" width="0.12" layer="51"/>
<wire x1="-2.1" y1="-1.8" x2="-2.1" y2="1.8" width="0.12" layer="51"/>
<wire x1="-2.1" y1="1.8" x2="2.1" y2="1.8" width="0.12" layer="51"/>
<wire x1="2.1" y1="1.8" x2="2.1" y2="-1.8" width="0.12" layer="51"/>
<smd name="VCC" x="-1.082040625" y="0" dx="0.9248125" dy="0.749809375" layer="1"/>
<smd name="NC" x="-1.082040625" y="-1.04648125" dx="0.9248125" dy="0.749809375" layer="1"/>
<smd name="GND" x="0.7112" y="0" dx="2.0248875" dy="2.799840625" layer="1"/>
<text x="0" y="2.435" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.435" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
<smd name="NC2" x="-1.082040625" y="1.04648125" dx="0.9248125" dy="0.749809375" layer="1"/>
</package>
</packages>
<packages3d>
<package3d name="DIODFN420X360X332-3N" urn="urn:adsk.eagle:package:31059650/1" type="model">
<description>DFN3, 4.20 X 3.60 X 3.33 mm body
&lt;p&gt;DFN3 package with body size 4.20 X 3.60 X 3.33 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIODFN420X360X332-3N"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="AQAA">
<description>AQAA-20</description>
<wire x1="-5.08" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<circle x="0" y="0" radius="2.54" width="0.254" layer="94"/>
<pin name="GND" x="10.16" y="0" length="middle" rot="R180"/>
<pin name="NC2" x="-10.16" y="2.54" visible="pad" length="middle" direction="nc"/>
<pin name="VCC" x="-10.16" y="0" length="middle"/>
<pin name="NC" x="-10.16" y="-2.54" visible="pad" length="middle" direction="nc"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AQAA">
<gates>
<gate name="G$1" symbol="AQAA" x="0" y="0"/>
</gates>
<devices>
<device name="AQAA" package="DIODFN420X360X332-3N">
<connects>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="NC" pad="NC"/>
<connect gate="G$1" pin="NC2" pad="NC2"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:31059650/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="amass">
<description>&lt;b&gt;AMASS PCB type connector&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by Suzaku Lab. Ltd. &lt;a href="mailto:support@suzakugiken.jp"&gt;support@suzakugiken.jp&lt;/a&gt;&lt;/author&gt;
&lt;p&gt;This is an unofficial library. If you notice a mistake, please contact the author.&lt;/p&gt;

&lt;p&gt;We, &lt;a href="https://suzakugiken.jp/"&gt;Suzaku Lab. Ltd.&lt;/a&gt;, are the authorized distributor in Japan.&lt;/p&gt;</description>
<packages>
<package name="XT30PW-F">
<description>AMASS XT30PW female type connector</description>
<pad name="-" x="-2.5" y="0" drill="1.8"/>
<pad name="+" x="2.5" y="0" drill="1.8"/>
<pad name="P$3" x="-5.5" y="5" drill="1.1"/>
<pad name="P$4" x="5.5" y="5" drill="1.1"/>
<wire x1="2" y1="5" x2="3" y2="5" width="0.127" layer="21"/>
<wire x1="2.5" y1="4.5" x2="2.5" y2="5.5" width="0.127" layer="21"/>
<wire x1="-6.65" y1="6" x2="-4.95" y2="6" width="0.127" layer="21"/>
<wire x1="-4.95" y1="6" x2="4.95" y2="6" width="0.127" layer="21"/>
<wire x1="4.95" y1="6" x2="6.65" y2="6" width="0.127" layer="21"/>
<wire x1="-6.65" y1="4" x2="-3.75" y2="4" width="0.127" layer="21"/>
<wire x1="-3.75" y1="4" x2="-1.25" y2="4" width="0.127" layer="21"/>
<wire x1="-1.25" y1="4" x2="6.65" y2="4" width="0.127" layer="21"/>
<wire x1="-6.65" y1="6" x2="-6.65" y2="4" width="0.127" layer="21"/>
<wire x1="6.65" y1="6" x2="6.65" y2="4" width="0.127" layer="21"/>
<wire x1="4.95" y1="8" x2="0.5" y2="8" width="0.127" layer="21"/>
<wire x1="0.5" y1="8" x2="0.4" y2="8" width="0.127" layer="21"/>
<wire x1="0.4" y1="8" x2="0.3" y2="8" width="0.127" layer="21"/>
<wire x1="0.3" y1="8" x2="0.2" y2="8" width="0.127" layer="21"/>
<wire x1="0.2" y1="8" x2="0.1" y2="8" width="0.127" layer="21"/>
<wire x1="0.1" y1="8" x2="0" y2="8" width="0.127" layer="21"/>
<wire x1="0" y1="8" x2="-0.1" y2="8" width="0.127" layer="21"/>
<wire x1="-0.1" y1="8" x2="-0.2" y2="8" width="0.127" layer="21"/>
<wire x1="-0.2" y1="8" x2="-0.3" y2="8" width="0.127" layer="21"/>
<wire x1="-0.3" y1="8" x2="-0.4" y2="8" width="0.127" layer="21"/>
<wire x1="-0.4" y1="8" x2="-0.5" y2="8" width="0.127" layer="21"/>
<wire x1="-0.5" y1="8" x2="-4.95" y2="8" width="0.127" layer="21"/>
<wire x1="-4.95" y1="8" x2="-4.95" y2="6" width="0.127" layer="21"/>
<wire x1="4.95" y1="8" x2="4.95" y2="6" width="0.127" layer="21"/>
<wire x1="4.45" y1="14" x2="-4.45" y2="14" width="0.127" layer="51"/>
<wire x1="-4.45" y1="14" x2="-4.45" y2="8" width="0.127" layer="51"/>
<wire x1="4.45" y1="14" x2="4.45" y2="8" width="0.127" layer="51"/>
<wire x1="-1.25" y1="2" x2="-1.25" y2="2.1" width="0.127" layer="21"/>
<wire x1="-1.25" y1="2.1" x2="-1.25" y2="2.2" width="0.127" layer="21"/>
<wire x1="-1.25" y1="2.2" x2="-1.25" y2="2.3" width="0.127" layer="21"/>
<wire x1="-1.25" y1="2.3" x2="-1.25" y2="2.4" width="0.127" layer="21"/>
<wire x1="-1.25" y1="2.4" x2="-1.25" y2="2.5" width="0.127" layer="21"/>
<wire x1="-1.25" y1="2.5" x2="-1.25" y2="2.6" width="0.127" layer="21"/>
<wire x1="-1.25" y1="2.6" x2="-1.25" y2="2.7" width="0.127" layer="21"/>
<wire x1="-1.25" y1="2.7" x2="-1.25" y2="2.8" width="0.127" layer="21"/>
<wire x1="-1.25" y1="2.8" x2="-1.25" y2="2.9" width="0.127" layer="21"/>
<wire x1="-1.25" y1="2.9" x2="-1.25" y2="3" width="0.127" layer="21"/>
<wire x1="-1.25" y1="3" x2="-1.25" y2="3.1" width="0.127" layer="21"/>
<wire x1="-1.25" y1="3.1" x2="-1.25" y2="3.2" width="0.127" layer="21"/>
<wire x1="-1.25" y1="3.2" x2="-1.25" y2="3.3" width="0.127" layer="21"/>
<wire x1="-1.25" y1="3.3" x2="-1.25" y2="3.4" width="0.127" layer="21"/>
<wire x1="-1.25" y1="3.4" x2="-1.25" y2="3.5" width="0.127" layer="21"/>
<wire x1="-1.25" y1="3.5" x2="-1.25" y2="3.6" width="0.127" layer="21"/>
<wire x1="-1.25" y1="3.6" x2="-1.25" y2="3.7" width="0.127" layer="21"/>
<wire x1="-1.25" y1="3.7" x2="-1.25" y2="3.8" width="0.127" layer="21"/>
<wire x1="-1.25" y1="3.8" x2="-1.25" y2="3.9" width="0.127" layer="21"/>
<wire x1="-1.25" y1="3.9" x2="-1.25" y2="4" width="0.127" layer="21"/>
<wire x1="-3.75" y1="4" x2="-3.75" y2="3.9" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.9" x2="-3.75" y2="3.8" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.8" x2="-3.75" y2="3.7" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.7" x2="-3.75" y2="3.6" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.6" x2="-3.75" y2="3.5" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.5" x2="-3.75" y2="3.4" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.4" x2="-3.75" y2="3.3" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.3" x2="-3.75" y2="3.2" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.2" x2="-3.75" y2="3.1" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.1" x2="-3.75" y2="3" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3" x2="-3.75" y2="2.9" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.9" x2="-3.75" y2="2.8" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.8" x2="-3.75" y2="2.7" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.7" x2="-3.75" y2="2.6" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.6" x2="-3.75" y2="2.5" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.5" x2="-3.75" y2="2.4" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.4" x2="-3.75" y2="2.3" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.3" x2="-3.75" y2="2.2" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.2" x2="-3.75" y2="2.1" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.1" x2="-3.75" y2="2" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2" x2="-3.5" y2="2" width="0.127" layer="21"/>
<wire x1="-3.5" y1="2" x2="-1.5" y2="2" width="0.127" layer="21"/>
<wire x1="-1.5" y1="2" x2="-1.25" y2="2" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.1" x2="-1.25" y2="2.1" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.2" x2="-1.25" y2="2.2" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.3" x2="-1.25" y2="2.3" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.4" x2="-1.25" y2="2.4" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.5" x2="-1.25" y2="2.5" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.6" x2="-1.25" y2="2.6" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.7" x2="-1.25" y2="2.7" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.8" x2="-1.25" y2="2.8" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.9" x2="-1.25" y2="2.9" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3" x2="-1.25" y2="3" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.1" x2="-1.25" y2="3.1" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.2" x2="-1.25" y2="3.2" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.3" x2="-1.25" y2="3.3" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.4" x2="-1.25" y2="3.4" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.5" x2="-1.25" y2="3.5" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.6" x2="-1.25" y2="3.6" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.7" x2="-1.25" y2="3.7" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.8" x2="-1.25" y2="3.8" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.9" x2="-1.25" y2="3.9" width="0.127" layer="21"/>
<wire x1="1.25" y1="4" x2="1.5" y2="4" width="0.127" layer="21"/>
<wire x1="3.5" y1="4" x2="3.75" y2="4" width="0.127" layer="21"/>
<wire x1="3.75" y1="2" x2="3.75" y2="2.1" width="0.127" layer="21"/>
<wire x1="3.75" y1="2.1" x2="3.75" y2="2.2" width="0.127" layer="21"/>
<wire x1="3.75" y1="2.2" x2="3.75" y2="2.3" width="0.127" layer="21"/>
<wire x1="3.75" y1="2.3" x2="3.75" y2="2.4" width="0.127" layer="21"/>
<wire x1="3.75" y1="2.4" x2="3.75" y2="2.5" width="0.127" layer="21"/>
<wire x1="3.75" y1="2.5" x2="3.75" y2="2.6" width="0.127" layer="21"/>
<wire x1="3.75" y1="2.6" x2="3.75" y2="2.7" width="0.127" layer="21"/>
<wire x1="3.75" y1="2.7" x2="3.75" y2="2.8" width="0.127" layer="21"/>
<wire x1="3.75" y1="2.8" x2="3.75" y2="2.9" width="0.127" layer="21"/>
<wire x1="3.75" y1="2.9" x2="3.75" y2="3" width="0.127" layer="21"/>
<wire x1="3.75" y1="3" x2="3.75" y2="3.1" width="0.127" layer="21"/>
<wire x1="3.75" y1="3.1" x2="3.75" y2="3.2" width="0.127" layer="21"/>
<wire x1="3.75" y1="3.2" x2="3.75" y2="3.3" width="0.127" layer="21"/>
<wire x1="3.75" y1="3.3" x2="3.75" y2="3.4" width="0.127" layer="21"/>
<wire x1="3.75" y1="3.4" x2="3.75" y2="3.5" width="0.127" layer="21"/>
<wire x1="3.75" y1="3.5" x2="3.75" y2="3.6" width="0.127" layer="21"/>
<wire x1="3.75" y1="3.6" x2="3.75" y2="3.7" width="0.127" layer="21"/>
<wire x1="3.75" y1="3.7" x2="3.75" y2="3.8" width="0.127" layer="21"/>
<wire x1="3.75" y1="3.8" x2="3.75" y2="3.9" width="0.127" layer="21"/>
<wire x1="3.75" y1="3.9" x2="3.75" y2="4" width="0.127" layer="21"/>
<wire x1="1.25" y1="4" x2="1.25" y2="3.9" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.9" x2="1.25" y2="3.8" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.8" x2="1.25" y2="3.7" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.7" x2="1.25" y2="3.6" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.6" x2="1.25" y2="3.5" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.5" x2="1.25" y2="3.4" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.4" x2="1.25" y2="3.3" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.3" x2="1.25" y2="3.2" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.2" x2="1.25" y2="3.1" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.1" x2="1.25" y2="3" width="0.127" layer="21"/>
<wire x1="1.25" y1="3" x2="1.25" y2="2.9" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.9" x2="1.25" y2="2.8" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.8" x2="1.25" y2="2.7" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.7" x2="1.25" y2="2.6" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.6" x2="1.25" y2="2.5" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.5" x2="1.25" y2="2.4" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.4" x2="1.25" y2="2.3" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.3" x2="1.25" y2="2.2" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.2" x2="1.25" y2="2.1" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.1" x2="1.25" y2="2" width="0.127" layer="21"/>
<wire x1="1.25" y1="2" x2="3.75" y2="2" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.1" x2="3.75" y2="2.1" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.2" x2="3.75" y2="2.2" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.3" x2="3.75" y2="2.3" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.4" x2="3.75" y2="2.4" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.5" x2="3.75" y2="2.5" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.6" x2="3.75" y2="2.6" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.7" x2="3.75" y2="2.7" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.8" x2="3.75" y2="2.8" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.9" x2="3.75" y2="2.9" width="0.127" layer="21"/>
<wire x1="1.25" y1="3" x2="3.75" y2="3" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.1" x2="3.75" y2="3.1" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.2" x2="3.75" y2="3.2" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.3" x2="3.75" y2="3.3" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.4" x2="3.75" y2="3.4" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.5" x2="3.75" y2="3.5" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.6" x2="3.75" y2="3.6" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.7" x2="3.75" y2="3.7" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.8" x2="3.75" y2="3.8" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.9" x2="3.75" y2="3.9" width="0.127" layer="21"/>
<wire x1="-3.5" y1="2" x2="-3.5" y2="1.9" width="0.127" layer="21"/>
<wire x1="-3.5" y1="1.9" x2="-3.5" y2="1.8" width="0.127" layer="21"/>
<wire x1="-3.5" y1="1.8" x2="-3.5" y2="1.7" width="0.127" layer="21"/>
<wire x1="-3.5" y1="1.7" x2="-3.5" y2="1.6" width="0.127" layer="21"/>
<wire x1="-3.5" y1="1.6" x2="-3.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="-3.5" y1="1.5" x2="-1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.5" x2="-1.5" y2="1.6" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.6" x2="-1.5" y2="1.7" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.7" x2="-1.5" y2="1.8" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.8" x2="-1.5" y2="1.9" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.9" x2="-1.5" y2="2" width="0.127" layer="21"/>
<wire x1="-3.5" y1="1.6" x2="-1.5" y2="1.6" width="0.127" layer="21"/>
<wire x1="-3.5" y1="1.7" x2="-1.5" y2="1.7" width="0.127" layer="21"/>
<wire x1="-3.5" y1="1.8" x2="-1.5" y2="1.8" width="0.127" layer="21"/>
<wire x1="-3.5" y1="1.9" x2="-1.5" y2="1.9" width="0.127" layer="21"/>
<wire x1="1.5" y1="2" x2="1.5" y2="1.9" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.9" x2="1.5" y2="1.8" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.8" x2="1.5" y2="1.7" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.7" x2="1.5" y2="1.6" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.6" x2="1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.5" x2="3.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="3.5" y1="1.5" x2="3.5" y2="1.6" width="0.127" layer="21"/>
<wire x1="3.5" y1="1.6" x2="3.5" y2="1.7" width="0.127" layer="21"/>
<wire x1="3.5" y1="1.7" x2="3.5" y2="1.8" width="0.127" layer="21"/>
<wire x1="3.5" y1="1.8" x2="3.5" y2="1.9" width="0.127" layer="21"/>
<wire x1="3.5" y1="1.9" x2="3.5" y2="2" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.6" x2="3.5" y2="1.6" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.7" x2="3.5" y2="1.7" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.8" x2="3.5" y2="1.8" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.9" x2="3.5" y2="1.9" width="0.127" layer="21"/>
<wire x1="-2.5" y1="0" x2="-2.5" y2="1.5" width="1.778" layer="51"/>
<wire x1="2.5" y1="0" x2="2.5" y2="1.5" width="1.778" layer="51"/>
<wire x1="-3.5" y1="14" x2="-3.5" y2="8" width="0.127" layer="51"/>
<text x="-5.08" y="-3.81" size="1.778" layer="25">&gt;NAME</text>
<text x="-5.08" y="14.605" size="1.778" layer="27">&gt;VALUE</text>
<wire x1="-0.5" y1="8" x2="-0.5" y2="14" width="0.127" layer="51"/>
<wire x1="-0.4" y1="8" x2="-0.4" y2="14" width="0.127" layer="51"/>
<wire x1="-0.3" y1="8" x2="-0.3" y2="14" width="0.127" layer="51"/>
<wire x1="-0.2" y1="8" x2="-0.2" y2="14" width="0.127" layer="51"/>
<wire x1="-0.1" y1="8" x2="-0.1" y2="14" width="0.127" layer="51"/>
<wire x1="0" y1="8" x2="0" y2="14" width="0.127" layer="51"/>
<wire x1="0.1" y1="8" x2="0.1" y2="14" width="0.127" layer="51"/>
<wire x1="0.2" y1="8" x2="0.2" y2="14" width="0.127" layer="51"/>
<wire x1="0.3" y1="8" x2="0.3" y2="14" width="0.127" layer="51"/>
<wire x1="0.4" y1="8" x2="0.4" y2="14" width="0.127" layer="51"/>
<wire x1="0.5" y1="8" x2="0.5" y2="14" width="0.127" layer="51"/>
</package>
<package name="XT30PW-M">
<description>AMASS XT30PW male type connector</description>
<pad name="+" x="-2.5" y="0" drill="1.8"/>
<pad name="-" x="2.5" y="0" drill="1.8"/>
<pad name="P$3" x="-5.5" y="10" drill="1.1"/>
<pad name="P$4" x="5.5" y="10" drill="1.1"/>
<wire x1="-3" y1="5" x2="-2" y2="5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="4.5" x2="-2.5" y2="5.5" width="0.127" layer="21"/>
<wire x1="-6.65" y1="11" x2="-4.95" y2="11" width="0.127" layer="21"/>
<wire x1="-4.95" y1="11" x2="4.95" y2="11" width="0.127" layer="21"/>
<wire x1="4.95" y1="11" x2="6.65" y2="11" width="0.127" layer="21"/>
<wire x1="-6.65" y1="9" x2="-4.95" y2="9" width="0.127" layer="21"/>
<wire x1="-4.95" y1="9" x2="4.95" y2="9" width="0.127" layer="21"/>
<wire x1="4.95" y1="9" x2="6.65" y2="9" width="0.127" layer="21"/>
<wire x1="-6.65" y1="11" x2="-6.65" y2="9" width="0.127" layer="21"/>
<wire x1="6.65" y1="11" x2="6.65" y2="9" width="0.127" layer="21"/>
<wire x1="-1.25" y1="2" x2="-1.25" y2="2.1" width="0.127" layer="21"/>
<wire x1="-1.25" y1="2.1" x2="-1.25" y2="2.2" width="0.127" layer="21"/>
<wire x1="-1.25" y1="2.2" x2="-1.25" y2="2.3" width="0.127" layer="21"/>
<wire x1="-1.25" y1="2.3" x2="-1.25" y2="2.4" width="0.127" layer="21"/>
<wire x1="-1.25" y1="2.4" x2="-1.25" y2="2.5" width="0.127" layer="21"/>
<wire x1="-1.25" y1="2.5" x2="-1.25" y2="2.6" width="0.127" layer="21"/>
<wire x1="-1.25" y1="2.6" x2="-1.25" y2="2.7" width="0.127" layer="21"/>
<wire x1="-1.25" y1="2.7" x2="-1.25" y2="2.8" width="0.127" layer="21"/>
<wire x1="-1.25" y1="2.8" x2="-1.25" y2="2.9" width="0.127" layer="21"/>
<wire x1="-1.25" y1="2.9" x2="-1.25" y2="3" width="0.127" layer="21"/>
<wire x1="-1.25" y1="3" x2="-1.25" y2="3.1" width="0.127" layer="21"/>
<wire x1="-1.25" y1="3.1" x2="-1.25" y2="3.2" width="0.127" layer="21"/>
<wire x1="-1.25" y1="3.2" x2="-1.25" y2="3.3" width="0.127" layer="21"/>
<wire x1="-1.25" y1="3.3" x2="-1.25" y2="3.4" width="0.127" layer="21"/>
<wire x1="-1.25" y1="3.4" x2="-1.25" y2="3.5" width="0.127" layer="21"/>
<wire x1="-1.25" y1="3.5" x2="-1.25" y2="3.6" width="0.127" layer="21"/>
<wire x1="-1.25" y1="3.6" x2="-1.25" y2="3.7" width="0.127" layer="21"/>
<wire x1="-1.25" y1="3.7" x2="-1.25" y2="3.8" width="0.127" layer="21"/>
<wire x1="-1.25" y1="3.8" x2="-1.25" y2="3.9" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.9" x2="-3.75" y2="3.8" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.8" x2="-3.75" y2="3.7" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.7" x2="-3.75" y2="3.6" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.6" x2="-3.75" y2="3.5" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.5" x2="-3.75" y2="3.4" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.4" x2="-3.75" y2="3.3" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.3" x2="-3.75" y2="3.2" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.2" x2="-3.75" y2="3.1" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.1" x2="-3.75" y2="3" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3" x2="-3.75" y2="2.9" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.9" x2="-3.75" y2="2.8" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.8" x2="-3.75" y2="2.7" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.7" x2="-3.75" y2="2.6" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.6" x2="-3.75" y2="2.5" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.5" x2="-3.75" y2="2.4" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.4" x2="-3.75" y2="2.3" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.3" x2="-3.75" y2="2.2" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.2" x2="-3.75" y2="2.1" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.1" x2="-3.75" y2="2" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2" x2="-3.5" y2="2" width="0.127" layer="21"/>
<wire x1="-3.5" y1="2" x2="-1.5" y2="2" width="0.127" layer="21"/>
<wire x1="-1.5" y1="2" x2="-1.25" y2="2" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.1" x2="-1.25" y2="2.1" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.2" x2="-1.25" y2="2.2" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.3" x2="-1.25" y2="2.3" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.4" x2="-1.25" y2="2.4" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.5" x2="-1.25" y2="2.5" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.6" x2="-1.25" y2="2.6" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.7" x2="-1.25" y2="2.7" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.8" x2="-1.25" y2="2.8" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.9" x2="-1.25" y2="2.9" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3" x2="-1.25" y2="3" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.1" x2="-1.25" y2="3.1" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.2" x2="-1.25" y2="3.2" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.3" x2="-1.25" y2="3.3" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.4" x2="-1.25" y2="3.4" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.5" x2="-1.25" y2="3.5" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.6" x2="-1.25" y2="3.6" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.7" x2="-1.25" y2="3.7" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.8" x2="-1.25" y2="3.8" width="0.127" layer="21"/>
<wire x1="1.25" y1="9" x2="1.5" y2="9" width="0.127" layer="21"/>
<wire x1="3.5" y1="9" x2="3.75" y2="9" width="0.127" layer="21"/>
<wire x1="3.75" y1="2" x2="3.75" y2="2.1" width="0.127" layer="21"/>
<wire x1="3.75" y1="2.1" x2="3.75" y2="2.2" width="0.127" layer="21"/>
<wire x1="3.75" y1="2.2" x2="3.75" y2="2.3" width="0.127" layer="21"/>
<wire x1="3.75" y1="2.3" x2="3.75" y2="2.4" width="0.127" layer="21"/>
<wire x1="3.75" y1="2.4" x2="3.75" y2="2.5" width="0.127" layer="21"/>
<wire x1="3.75" y1="2.5" x2="3.75" y2="2.6" width="0.127" layer="21"/>
<wire x1="3.75" y1="2.6" x2="3.75" y2="2.7" width="0.127" layer="21"/>
<wire x1="3.75" y1="2.7" x2="3.75" y2="2.8" width="0.127" layer="21"/>
<wire x1="3.75" y1="2.8" x2="3.75" y2="2.9" width="0.127" layer="21"/>
<wire x1="3.75" y1="2.9" x2="3.75" y2="3" width="0.127" layer="21"/>
<wire x1="3.75" y1="3" x2="3.75" y2="3.1" width="0.127" layer="21"/>
<wire x1="3.75" y1="3.1" x2="3.75" y2="3.2" width="0.127" layer="21"/>
<wire x1="3.75" y1="3.2" x2="3.75" y2="3.3" width="0.127" layer="21"/>
<wire x1="3.75" y1="3.3" x2="3.75" y2="3.4" width="0.127" layer="21"/>
<wire x1="3.75" y1="3.4" x2="3.75" y2="3.5" width="0.127" layer="21"/>
<wire x1="3.75" y1="3.5" x2="3.75" y2="3.6" width="0.127" layer="21"/>
<wire x1="3.75" y1="3.6" x2="3.75" y2="3.7" width="0.127" layer="21"/>
<wire x1="3.75" y1="3.7" x2="3.75" y2="3.8" width="0.127" layer="21"/>
<wire x1="3.75" y1="3.8" x2="3.75" y2="3.9" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.9" x2="1.25" y2="3.8" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.8" x2="1.25" y2="3.7" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.7" x2="1.25" y2="3.6" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.6" x2="1.25" y2="3.5" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.5" x2="1.25" y2="3.4" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.4" x2="1.25" y2="3.3" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.3" x2="1.25" y2="3.2" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.2" x2="1.25" y2="3.1" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.1" x2="1.25" y2="3" width="0.127" layer="21"/>
<wire x1="1.25" y1="3" x2="1.25" y2="2.9" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.9" x2="1.25" y2="2.8" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.8" x2="1.25" y2="2.7" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.7" x2="1.25" y2="2.6" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.6" x2="1.25" y2="2.5" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.5" x2="1.25" y2="2.4" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.4" x2="1.25" y2="2.3" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.3" x2="1.25" y2="2.2" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.2" x2="1.25" y2="2.1" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.1" x2="1.25" y2="2" width="0.127" layer="21"/>
<wire x1="1.25" y1="2" x2="3.75" y2="2" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.1" x2="3.75" y2="2.1" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.2" x2="3.75" y2="2.2" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.3" x2="3.75" y2="2.3" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.4" x2="3.75" y2="2.4" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.5" x2="3.75" y2="2.5" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.6" x2="3.75" y2="2.6" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.7" x2="3.75" y2="2.7" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.8" x2="3.75" y2="2.8" width="0.127" layer="21"/>
<wire x1="1.25" y1="2.9" x2="3.75" y2="2.9" width="0.127" layer="21"/>
<wire x1="1.25" y1="3" x2="3.75" y2="3" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.1" x2="3.75" y2="3.1" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.2" x2="3.75" y2="3.2" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.3" x2="3.75" y2="3.3" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.4" x2="3.75" y2="3.4" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.5" x2="3.75" y2="3.5" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.6" x2="3.75" y2="3.6" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.7" x2="3.75" y2="3.7" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.8" x2="3.75" y2="3.8" width="0.127" layer="21"/>
<wire x1="1.25" y1="3.9" x2="3.75" y2="3.9" width="0.127" layer="21"/>
<wire x1="-3.5" y1="2" x2="-3.5" y2="1.9" width="0.127" layer="21"/>
<wire x1="-3.5" y1="1.9" x2="-3.5" y2="1.8" width="0.127" layer="21"/>
<wire x1="-3.5" y1="1.8" x2="-3.5" y2="1.7" width="0.127" layer="21"/>
<wire x1="-3.5" y1="1.7" x2="-3.5" y2="1.6" width="0.127" layer="21"/>
<wire x1="-3.5" y1="1.6" x2="-3.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="-3.5" y1="1.5" x2="-1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.5" x2="-1.5" y2="1.6" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.6" x2="-1.5" y2="1.7" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.7" x2="-1.5" y2="1.8" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.8" x2="-1.5" y2="1.9" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.9" x2="-1.5" y2="2" width="0.127" layer="21"/>
<wire x1="-3.5" y1="1.6" x2="-1.5" y2="1.6" width="0.127" layer="21"/>
<wire x1="-3.5" y1="1.7" x2="-1.5" y2="1.7" width="0.127" layer="21"/>
<wire x1="-3.5" y1="1.8" x2="-1.5" y2="1.8" width="0.127" layer="21"/>
<wire x1="-3.5" y1="1.9" x2="-1.5" y2="1.9" width="0.127" layer="21"/>
<wire x1="1.5" y1="2" x2="1.5" y2="1.9" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.9" x2="1.5" y2="1.8" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.8" x2="1.5" y2="1.7" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.7" x2="1.5" y2="1.6" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.6" x2="1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.5" x2="3.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="3.5" y1="1.5" x2="3.5" y2="1.6" width="0.127" layer="21"/>
<wire x1="3.5" y1="1.6" x2="3.5" y2="1.7" width="0.127" layer="21"/>
<wire x1="3.5" y1="1.7" x2="3.5" y2="1.8" width="0.127" layer="21"/>
<wire x1="3.5" y1="1.8" x2="3.5" y2="1.9" width="0.127" layer="21"/>
<wire x1="3.5" y1="1.9" x2="3.5" y2="2" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.6" x2="3.5" y2="1.6" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.7" x2="3.5" y2="1.7" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.8" x2="3.5" y2="1.8" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.9" x2="3.5" y2="1.9" width="0.127" layer="21"/>
<wire x1="-2.5" y1="0" x2="-2.5" y2="1.5" width="1.778" layer="51"/>
<wire x1="2.5" y1="0" x2="2.5" y2="1.5" width="1.778" layer="51"/>
<text x="-5.08" y="-3.81" size="1.778" layer="25">&gt;NAME</text>
<text x="-5.3" y="13.5" size="1.778" layer="27">&gt;VALUE</text>
<wire x1="-4.95" y1="13" x2="4.95" y2="13" width="0.127" layer="21"/>
<wire x1="-4.95" y1="13" x2="-4.95" y2="11" width="0.127" layer="21"/>
<wire x1="4.95" y1="13" x2="4.95" y2="11" width="0.127" layer="21"/>
<wire x1="-4.95" y1="4" x2="-3.75" y2="4" width="0.127" layer="21"/>
<wire x1="-3.75" y1="4" x2="-1.25" y2="4" width="0.127" layer="21"/>
<wire x1="-1.25" y1="4" x2="1.25" y2="4" width="0.127" layer="21"/>
<wire x1="1.25" y1="4" x2="3.75" y2="4" width="0.127" layer="21"/>
<wire x1="3.75" y1="4" x2="4.95" y2="4" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.9" x2="-1.25" y2="3.9" width="0.127" layer="21"/>
<wire x1="-3.75" y1="4" x2="-3.75" y2="3.9" width="0.127" layer="21"/>
<wire x1="-1.25" y1="3.9" x2="-1.25" y2="4" width="0.127" layer="21"/>
<wire x1="1.25" y1="4" x2="1.25" y2="3.9" width="0.127" layer="21"/>
<wire x1="3.75" y1="3.9" x2="3.75" y2="4" width="0.127" layer="21"/>
<wire x1="-4.95" y1="9" x2="-4.95" y2="4" width="0.127" layer="21"/>
<wire x1="4.95" y1="9" x2="4.95" y2="4" width="0.127" layer="21"/>
<wire x1="-4.95" y1="11" x2="-4.95" y2="9" width="0.127" layer="51"/>
<wire x1="4.95" y1="11" x2="4.95" y2="9" width="0.127" layer="51"/>
</package>
<package name="XT30UPB-F">
<description>AMASS XT30UPB female type connector</description>
<text x="-3.5" y="-5" size="1.778" layer="25">&gt;NAME</text>
<text x="-4.5" y="3" size="1.778" layer="27">&gt;VALUE</text>
<pad name="-" x="-2.5" y="0" drill="1.8"/>
<pad name="+" x="2.5" y="0" drill="1.8"/>
<wire x1="-5.1" y1="0.6" x2="-5.1" y2="-0.6" width="0.127" layer="21"/>
<wire x1="-5.1" y1="-0.6" x2="-3.1" y2="-2.6" width="0.127" layer="21" curve="90"/>
<wire x1="-3.1" y1="-2.6" x2="5.1" y2="-2.6" width="0.127" layer="21"/>
<wire x1="-5.1" y1="0.6" x2="-3.1" y2="2.6" width="0.127" layer="21" curve="-90"/>
<wire x1="-3.1" y1="2.6" x2="5.1" y2="2.6" width="0.127" layer="21"/>
<wire x1="5.1" y1="2.6" x2="5.1" y2="-2.6" width="0.127" layer="21"/>
<wire x1="-2.5" y1="2" x2="-3.4" y2="2" width="0.127" layer="21"/>
<wire x1="-3.4" y1="2" x2="-4.4" y2="1" width="0.127" layer="21"/>
<wire x1="-4.4" y1="1" x2="-4.4" y2="-1" width="0.127" layer="21"/>
<wire x1="-4.4" y1="-1" x2="-3.4" y2="-2" width="0.127" layer="21"/>
<wire x1="-3.4" y1="-2" x2="-2.5" y2="-2" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-2" x2="-0.5" y2="0" width="0.127" layer="21" curve="90"/>
<wire x1="0.5" y1="0" x2="2.5" y2="-2" width="0.127" layer="21" curve="90"/>
<wire x1="2.5" y1="-2" x2="4.4" y2="-2" width="0.127" layer="21"/>
<wire x1="4.4" y1="-2" x2="4.4" y2="2" width="0.127" layer="21"/>
<wire x1="4.4" y1="2" x2="2.5" y2="2" width="0.127" layer="21"/>
<wire x1="2.5" y1="2" x2="0.5" y2="0" width="0.127" layer="21" curve="90"/>
<wire x1="-0.5" y1="0" x2="-2.5" y2="2" width="0.127" layer="21" curve="90"/>
<wire x1="-0.5" y1="0" x2="0.5" y2="0" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-2" x2="2.5" y2="-2" width="0.127" layer="21"/>
<wire x1="-2.5" y1="2" x2="2.5" y2="2" width="0.127" layer="21"/>
<circle x="-2.5" y="0" radius="1.6" width="0.127" layer="51"/>
<circle x="2.5" y="0" radius="1.6" width="0.127" layer="51"/>
<wire x1="5.5" y1="0" x2="6.5" y2="0" width="0.127" layer="21"/>
<wire x1="6" y1="0.5" x2="6" y2="-0.5" width="0.127" layer="21"/>
</package>
<package name="XT30UPB-M">
<description>AMASS XT30UPB male type connector</description>
<text x="-3.5" y="-5" size="1.778" layer="25">&gt;NAME</text>
<text x="-4.5" y="3" size="1.778" layer="27">&gt;VALUE</text>
<pad name="-" x="-2.5" y="0" drill="1.8"/>
<pad name="+" x="2.5" y="0" drill="1.8"/>
<wire x1="-5.1" y1="0.6" x2="-5.1" y2="-0.6" width="0.127" layer="21"/>
<wire x1="-5.1" y1="-0.6" x2="-3.1" y2="-2.6" width="0.127" layer="21" curve="90"/>
<wire x1="-3.1" y1="-2.6" x2="5.1" y2="-2.6" width="0.127" layer="21"/>
<wire x1="-5.1" y1="0.6" x2="-3.1" y2="2.6" width="0.127" layer="21" curve="-90"/>
<wire x1="-3.1" y1="2.6" x2="5.1" y2="2.6" width="0.127" layer="21"/>
<wire x1="5.1" y1="2.6" x2="5.1" y2="-2.6" width="0.127" layer="21"/>
<wire x1="-3.5" y1="2.25" x2="-4.75" y2="1" width="0.127" layer="21"/>
<wire x1="-4.75" y1="1" x2="-4.75" y2="-1" width="0.127" layer="21"/>
<wire x1="-4.75" y1="-1" x2="-3.5" y2="-2.25" width="0.127" layer="21"/>
<wire x1="4.75" y1="-2.25" x2="4.75" y2="2.25" width="0.127" layer="21"/>
<wire x1="-3.5" y1="-2.25" x2="4.75" y2="-2.25" width="0.127" layer="21"/>
<wire x1="-3.5" y1="2.25" x2="4.75" y2="2.25" width="0.127" layer="21"/>
<wire x1="5.5" y1="0" x2="6.5" y2="0" width="0.127" layer="21"/>
<wire x1="6" y1="0.5" x2="6" y2="-0.5" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="CON-2">
<wire x1="-5.08" y1="5.715" x2="-5.08" y2="-4.318" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="-4.318" x2="-3.048" y2="-6.35" width="0.4064" layer="94"/>
<wire x1="-3.048" y1="-6.35" x2="-0.762" y2="-6.35" width="0.4064" layer="94"/>
<wire x1="-0.762" y1="-6.35" x2="1.27" y2="-4.318" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-4.318" x2="1.27" y2="5.715" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="5.715" x2="1.27" y2="5.715" width="0.4064" layer="94"/>
<text x="-3.81" y="-8.89" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="7.112" size="1.778" layer="95">&gt;NAME</text>
<pin name="+" x="5.08" y="2.54" visible="pad" length="middle" direction="pwr" rot="R180"/>
<pin name="-" x="5.08" y="-2.54" visible="pad" length="middle" direction="pwr" rot="R180"/>
<wire x1="0" y1="2.54" x2="-1.778" y2="2.54" width="1.27" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.778" y2="-2.54" width="1.27" layer="94"/>
<wire x1="-4.445" y1="2.54" x2="-2.921" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-3.683" y1="3.302" x2="-3.683" y2="1.778" width="0.4064" layer="94"/>
<wire x1="-4.445" y1="-2.54" x2="-2.921" y2="-2.54" width="0.4064" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="XT30" prefix="CON">
<description>&lt;b&gt;AMASS XT30 connector&lt;/b&gt;

&lt;p&gt;Store in Japan&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://szc.jp/ams-xt30"&gt;AMASS XT30 connector&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="CON-2" x="0" y="0"/>
</gates>
<devices>
<device name="PW-F" package="XT30PW-F">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PW-M" package="XT30PW-M">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="UPB-F" package="XT30UPB-F">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="UPB-M" package="XT30UPB-M">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Switch" urn="urn:adsk.eagle:library:11396471">
<description>&lt;h3&gt; PCBLayout.com - Frequently Used &lt;i&gt;Switches&lt;/i&gt;&lt;/h3&gt;

Visit us at &lt;a href="http://www.PCBLayout.com"&gt;PCBLayout.com&lt;/a&gt; for quick and hassle-free PCB Layout/Manufacturing ordering experience.
&lt;BR&gt;
&lt;BR&gt;
This library has been generated by our experienced pcb layout engineers using current IPC and/or industry standards. We &lt;b&gt;believe&lt;/b&gt; the content to be accurate, complete and current. But, this content is provided as a courtesy and &lt;u&gt;user assumes all risk and responsiblity of it's usage&lt;/u&gt;.
&lt;BR&gt;
&lt;BR&gt;
Feel free to contact us at &lt;a href="mailto:Support@PCBLayout.com"&gt;Support@PCBLayout.com&lt;/a&gt; if you have any questions/concerns regarding any of our content or services.</description>
<packages>
<package name="ES02MSABE" urn="urn:adsk.eagle:footprint:19992262/4" library_version="4">
<smd name="3" x="-2.54" y="0" dx="0.89" dy="2.08" layer="1" rot="R180"/>
<smd name="1" x="2.54" y="0" dx="0.89" dy="2.08" layer="1" rot="R180"/>
<smd name="S2" x="-5.3775" y="-8.92" dx="2.325" dy="1.52" layer="1" rot="R180"/>
<smd name="S1" x="5.3775" y="-8.92" dx="2.325" dy="1.52" layer="1"/>
<wire x1="5.015" y1="-10.19" x2="1.01" y2="-10.19" width="0.127" layer="51"/>
<wire x1="1.01" y1="-10.19" x2="-2.24" y2="-10.19" width="0.127" layer="51"/>
<wire x1="-2.24" y1="-10.19" x2="-5.015" y2="-10.19" width="0.127" layer="51"/>
<wire x1="-5.015" y1="-10.19" x2="-5.015" y2="-1.05" width="0.127" layer="51"/>
<wire x1="-5.015" y1="-1.05" x2="5.015" y2="-1.05" width="0.127" layer="51"/>
<wire x1="5.015" y1="-1.05" x2="5.015" y2="-10.19" width="0.127" layer="51"/>
<wire x1="5.015" y1="-10.19" x2="-5.015" y2="-10.19" width="0.127" layer="21"/>
<wire x1="5.015" y1="-7.86" x2="5.015" y2="-1.05" width="0.127" layer="21"/>
<wire x1="5.015" y1="-1.05" x2="3.5" y2="-1.05" width="0.127" layer="21"/>
<wire x1="-3.5" y1="-1.05" x2="-5.015" y2="-1.05" width="0.127" layer="21"/>
<wire x1="-5.015" y1="-1.05" x2="-5.015" y2="-7.86" width="0.127" layer="21"/>
<wire x1="-2.24" y1="-10.19" x2="-2.24" y2="-13.26" width="0.127" layer="51"/>
<wire x1="-2.24" y1="-13.26" x2="1.01" y2="-13.26" width="0.127" layer="51"/>
<wire x1="1.01" y1="-13.26" x2="1.01" y2="-10.19" width="0.127" layer="51"/>
<wire x1="5.6" y1="1.49" x2="-5.35" y2="1.49" width="0.05" layer="39"/>
<wire x1="-5.35" y1="-10.51" x2="-2.62" y2="-10.51" width="0.05" layer="39"/>
<wire x1="-2.62" y1="-10.51" x2="-2.62" y2="-13.61" width="0.05" layer="39"/>
<wire x1="-2.62" y1="-13.61" x2="1.38" y2="-13.61" width="0.05" layer="39"/>
<wire x1="1.38" y1="-13.61" x2="1.38" y2="-10.51" width="0.05" layer="39"/>
<wire x1="1.38" y1="-10.51" x2="5.35" y2="-10.51" width="0.05" layer="39"/>
<wire x1="-6.95" y1="-10.05" x2="-6.95" y2="-7.8" width="0.05" layer="39"/>
<wire x1="-6.95" y1="-7.8" x2="-5.35" y2="-7.8" width="0.05" layer="39"/>
<wire x1="-5.35" y1="-7.8" x2="-5.35" y2="1.49" width="0.05" layer="39"/>
<wire x1="7" y1="-10.05" x2="7" y2="-7.8" width="0.05" layer="39"/>
<wire x1="7" y1="-7.8" x2="5.6" y2="-7.8" width="0.05" layer="39"/>
<wire x1="5.6" y1="-7.8" x2="5.6" y2="1.49" width="0.05" layer="39"/>
<wire x1="-6.95" y1="-10.05" x2="-5.35" y2="-10.05" width="0.05" layer="39"/>
<wire x1="-5.35" y1="-10.05" x2="-5.35" y2="-10.51" width="0.05" layer="39"/>
<wire x1="7" y1="-10.05" x2="5.35" y2="-10.05" width="0.05" layer="39"/>
<wire x1="5.35" y1="-10.05" x2="5.35" y2="-10.51" width="0.05" layer="39"/>
<circle x="2.413" y="2.017" radius="0.1" width="0.2" layer="21"/>
<text x="-5.08" y="2.54" size="1.778" layer="25">&gt;NAME</text>
<text x="-5.08" y="-16.51" size="1.778" layer="27">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="ES02MSABE" urn="urn:adsk.eagle:package:19992278/5" type="model" library_version="4">
<packageinstances>
<packageinstance name="ES02MSABE"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="SPST_SH" urn="urn:adsk.eagle:symbol:20296286/1" library_version="4">
<wire x1="3.175" y1="0" x2="1.905" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="0" x2="-3.175" y2="0" width="0.1524" layer="94"/>
<wire x1="1.9" y1="0" x2="-1.9" y2="1.1" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.286" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.286" y1="-2.54" x2="-2.286" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="-3.048" y1="-3.048" x2="-1.524" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="-2.794" y1="-3.302" x2="-1.778" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-3.556" x2="-2.032" y2="-3.556" width="0.1524" layer="94"/>
<text x="-3.81" y="2.54" size="1.778" layer="95" rot="R180" align="top-right">&gt;NAME</text>
<text x="-3.81" y="-7.62" size="1.778" layer="96" rot="R180" align="top-right">&gt;VALUE</text>
<pin name="3" x="5.08" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas"/>
<pin name="SH" x="-5.08" y="-2.54" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ES02MSABE" urn="urn:adsk.eagle:component:20296298/1" prefix="S" library_version="4">
<description>&lt;h3&gt; SWITCH SLIDE SPST 0.4VA 20V &lt;/h3&gt;
&lt;BR&gt;
&lt;a href="https://www.ckswitches.com/media/1421/es.pdf"&gt; Manufacturer's datasheet&lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="SPST_SH" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ES02MSABE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="SH" pad="S1 S2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:19992278/5"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="CREATED_BY" value="PCBLayout.com" constant="no"/>
<attribute name="DIGIKEY_PARTNO" value="CKN1813TR-ND" constant="no"/>
<attribute name="MANUFACTURER" value="C&amp;K" constant="no"/>
<attribute name="MPN" value="ES02MSABE" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$1" library="AQAA" deviceset="AQAA" device="AQAA" package3d_urn="urn:adsk.eagle:package:31059650/1" value="AQAA"/>
<part name="U$2" library="AQAA" deviceset="AQAA" device="AQAA" package3d_urn="urn:adsk.eagle:package:31059650/1" value="AQAA"/>
<part name="CON1" library="amass" deviceset="XT30" device="UPB-M"/>
<part name="S1" library="Switch" library_urn="urn:adsk.eagle:library:11396471" deviceset="ES02MSABE" device="" package3d_urn="urn:adsk.eagle:package:19992278/5"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U$1" gate="G$1" x="2.54" y="20.32" smashed="yes" rot="R90"/>
<instance part="U$2" gate="G$1" x="-7.62" y="20.32" smashed="yes" rot="R90"/>
<instance part="CON1" gate="G$1" x="22.86" y="20.32" smashed="yes" rot="R180">
<attribute name="VALUE" x="26.67" y="29.21" size="1.778" layer="96" rot="R180"/>
<attribute name="NAME" x="26.67" y="13.208" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="S1" gate="G$1" x="25.4" y="0" smashed="yes" rot="R180">
<attribute name="NAME" x="29.21" y="-2.54" size="1.778" layer="95" align="top-right"/>
<attribute name="VALUE" x="29.21" y="7.62" size="1.778" layer="96" align="top-right"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="GND"/>
<pinref part="U$1" gate="G$1" pin="GND"/>
<wire x1="-7.62" y1="30.48" x2="-2.54" y2="30.48" width="0.1524" layer="91"/>
<junction x="-2.54" y="30.48"/>
<wire x1="-2.54" y1="30.48" x2="2.54" y2="30.48" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="-"/>
<wire x1="17.78" y1="22.86" x2="17.78" y2="35.56" width="0.1524" layer="91"/>
<wire x1="17.78" y1="35.56" x2="-2.54" y2="35.56" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="35.56" x2="-2.54" y2="30.48" width="0.1524" layer="91"/>
<wire x1="17.78" y1="22.86" x2="15.24" y2="22.86" width="0.1524" layer="91"/>
<junction x="17.78" y="22.86"/>
<wire x1="15.24" y1="22.86" x2="15.24" y2="10.16" width="0.1524" layer="91"/>
<wire x1="15.24" y1="10.16" x2="33.02" y2="10.16" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="SH"/>
<wire x1="33.02" y1="10.16" x2="33.02" y2="2.54" width="0.1524" layer="91"/>
<wire x1="33.02" y1="2.54" x2="30.48" y2="2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="VCC"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="5.08" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="VCC"/>
<wire x1="-7.62" y1="5.08" x2="-2.54" y2="5.08" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="5.08" x2="2.54" y2="5.08" width="0.1524" layer="91"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="10.16" width="0.1524" layer="91"/>
<junction x="-2.54" y="5.08"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="0" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="3"/>
<wire x1="-2.54" y1="0" x2="20.32" y2="0" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="S1" gate="G$1" pin="1"/>
<wire x1="30.48" y1="0" x2="35.56" y2="0" width="0.1524" layer="91"/>
<wire x1="35.56" y1="0" x2="35.56" y2="12.7" width="0.1524" layer="91"/>
<wire x1="35.56" y1="12.7" x2="17.78" y2="12.7" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="+"/>
<wire x1="17.78" y1="12.7" x2="17.78" y2="17.78" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="104,1,17.78,17.78,CON1,+,N$3,,,"/>
<approved hash="104,1,17.78,22.86,CON1,-,N$1,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
