/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : fsm.h
 * @brief          : Header for fsm.c file.
 *                   This file contains the common defines of the application.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __AVENA_PARAMETERS_H
#define __AVENA_PARAMETERS_H

#ifdef __cplusplus
extern "C" {
#endif

typedef enum Heartbeat {
	HEARTBEAT_DISABLED = 0, /**< @brief Starting uC.*/
	HEARTBEAT_ENABLED = 1
} Heartbeat_t;

typedef enum Security {
	SECURITY_DISABLED = 0, /**< @brief Starting uC.*/
	SECURITY_ENABLED = 1
} Security_t;

typedef enum Node_Type {
	UNKNOWN = 0, /**< @brief Starting uC.*/
	JOINT_SMALL = 1, JOINT_BIG = 2, GRIPPER_ONE = 3, GRIPPER_TWO = 4
} Node_Type_t;

/* Includes ------------------------------------------------------------------*/

/* Private includes ----------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/
typedef struct {
	Node_Type_t node_type;
	uint8_t can_node_id;
	Heartbeat_t hartbeat;
	Security_t security;
} __attribute__ ((packed)) NodeStatus_t;

extern NodeStatus_t g_node_status;

/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/

/* Exported functions prototypes ---------------------------------------------*/

/* Private defines -----------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif /* __AVENA_PARAMETERS_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
