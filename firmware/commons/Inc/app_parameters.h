/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : fsm.h
 * @brief          : Header for fsm.c file.
 *                   This file contains the common defines of the application.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __APP_PARAMETERS_H
#define __APP_PARAMETERS_H

#ifdef __cplusplus
extern "C" {
#endif

#include <math.h>

#define LOW_RES_THRESHOLD   ((uint16_t) 0x5500u)
#define HALL_COUNTER_RESET  ((uint16_t) 0u)
#define S16_120_PHASE_SHIFT (int16_t)(65536 / 3)
#define S16_60_PHASE_SHIFT  (int16_t)(65536 / 6)
#define STATE_0 (uint8_t)0
#define STATE_1 (uint8_t)1
#define STATE_2 (uint8_t)2
#define STATE_3 (uint8_t)3
#define STATE_4 (uint8_t)4
#define STATE_5 (uint8_t)5
#define STATE_6 (uint8_t)6
#define STATE_7 (uint8_t)7
#define NEGATIVE          (int8_t)-1
#define POSITIVE          (int8_t)1


#define SPEED_DATA_SIZE (uint8_t) 1000

#define CURRENT_TORQUE_DATA_SIZE (uint8_t) 50
#define CURRENT_TEMPERATURE_DATA_SIZE (uint8_t) 50

#define MAX_READABLE_CURRENT 33.0

#define HALL_IMPULSE_ANGLE_IN_RAD 0.07479982508547

#define  JOINT_NO_ERROR  				(uint8_t)(0x00u)      /**< @brief No error.*/
#define  JOINT_POSITION_ENCODER_FAILED  (uint8_t)(0x01u)
#define  JOINT_MC_FAILED  				(uint8_t)(0x02u)

#define  JOINT_NO_WARNING  				(uint8_t)(0x00u)      /**< @brief No error.*/
#define  JOINT_POSITION_NOT_ACCURATE	(uint8_t)(0x01u)
#define  JOINT_OUTSIDE_WORKING_AREA		(uint8_t)(0x02u)

//#define  MC_NO_FAULTS  (uint16_t)(0x0000u)     /**< @brief No error.*/
//#define  MC_FOC_DURATION  (uint16_t)(0x0001u)  /**< @brief Error: FOC rate to high.*/
//#define  MC_OVER_VOLT  (uint16_t)(0x0002u)     /**< @brief Error: Software over voltage.*/
//#define  MC_UNDER_VOLT  (uint16_t)(0x0004u)    /**< @brief Error: Software under voltage.*/
//#define  MC_OVER_TEMP  (uint16_t)(0x0008u)     /**< @brief Error: Software over temperature.*/
//#define  MC_START_UP  (uint16_t)(0x0010u)      /**< @brief Error: Startup failed.*/
//#define  MC_SPEED_FDBK  (uint16_t)(0x0020u)    /**< @brief Error: Speed feedback.*/
//#define  MC_BREAK_IN  (uint16_t)(0x0040u)      /**< @brief Error: Emergency input (Over current).*/
//#define  MC_SW_ERROR  (uint16_t)(0x0080u)      /**< @brief Software Error.*/

//typedef enum Heartbeat {
//	HEARTBEAT_DISABLED = 0, /**< @brief Starting uC.*/
//	HEARTBEAT_ENABLED = 1
//} Heartbeat_t;

//typedef enum Security {
//	SECURITY_DISABLED = 0, /**< @brief Starting uC.*/
//	SECURITY_ENABLED = 1
//} Security_t;

//typedef enum Node_Type {
//	UNKNOWN = 0, /**< @brief Starting uC.*/
//	JOINT_SMALL = 1, JOINT_BIG = 2, GRIPPER_ONE = 3, GRIPPER_TWO = 4
//} Node_Type_t;

/* Includes ------------------------------------------------------------------*/

/* Private includes ----------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/
//typedef struct {
//	uint8_t joint_id;
//} __attribute__ ((packed)) NodeStatus_t;
//
//#define NodeStatus_INIT {0}

typedef enum Joint_Calibratation_State {
	JOINT_NOT_CALIBRATED = 0, /**< @brief Starting uC.*/
	JOINT_CALIBRATED = 1
} Joint_Calibratation_State_t;

typedef enum Encoder_Position_State {
	POSITION_UNKNOWN = 0, /**< @brief Starting uC.*/
	POSITION_APROXIMATED = 1,
//	POSITION_PRECISE = 2,
//	POSITION_VERY_PRECISE = 3,
	POSITION_ACCURATE = 4,
	POSITION_ESTIMATION_FAILED = 255
} Encoder_Position_State_t;

typedef enum Joint_Position_State {
	POSITION_UNDER_WORKING_AREA = -1,
	POSITION_IN_WORKING_AREA = 0, /**< @brief Starting uC.*/
	POSITION_OVER_WORKING_AREA = 1
} Joint_Position_State_t;

typedef struct {
	bool termistor_exists;
	bool ma730_exists;
	bool working_area_constrain; /**< @brief settings to enable/disable constrain */
	Joint_Calibratation_State_t calibration_state;
	uint16_t gear_ratio;
	uint16_t pole_pairs;
	uint16_t maximum_electrical_rotations;
	uint16_t number_of_sectors;
	uint8_t  zero_hall_state;
	uint16_t zero_electric_rotation;
	uint16_t hall_working_area; // +- hall encoder ticks of two direction torque
	uint16_t calibration_sector_size; // in electrical rotations
	uint16_t calibration_table_size;
	uint16_t calibration_table_1[1680];
	uint16_t calibration_table_2[1680];
} __attribute__ ((packed)) JointConfiguration_t;

typedef struct {
	int16_t goal_torque;
	double  goal_motor_torque_in_nm;
	double  goal_joint_torque_in_nm;
//	int16_t _last_goal_torque;
} __attribute__ ((packed)) MotorCommand_t;

typedef struct {
	Encoder_Position_State_t encoder_position_state;


	int32_t previous_hall_encoder_position;
	int32_t current_hall_encoder_position;
	double current_joint_position_in_rad;

	Joint_Position_State_t current_joint_position;

	int32_t current_electric_rotation;

	uint8_t current_hall_state;

	double  current_hall_encoder_speed;

	double  current_motor_speed_in_rads;
//	double  current_motor_speed_in_rpm;
	int16_t current_motor_speed_in_rads_int;

	double  current_joint_speed_in_rads;
//	double  current_joint_speed_in_rpm;
	int16_t current_joint_speed_in_rads_int;

	int16_t current_torque;
	double  current_motor_torque_in_nm;
	double  current_joint_torque_in_nm;

	uint8_t current_temperature;
	double  current_voltage;

	uint16_t current_ma730_value;

	uint8_t errors;
	uint8_t warnings;

	State_t stm_state_motor;
	uint16_t mc_current_faults_motor;
	uint16_t mc_occured_faults_motor;

	int16_t _current_hall_speed_data[SPEED_DATA_SIZE];
	uint8_t _current_hall_speed_index;

	int16_t _current_torque_data[CURRENT_TORQUE_DATA_SIZE];
	uint8_t _current_torque_index;
} __attribute__ ((packed)) MotorStatus_t;

extern NodeStatus_t 		g_node_status;
extern JointConfiguration_t g_joint_configuration;
extern MotorCommand_t 		g_motor_command;
extern MotorStatus_t 		g_motor_status;

/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/

/* Exported functions prototypes ---------------------------------------------*/

/* Private defines -----------------------------------------------------------*/
//void RecalculateMotorState(uint16_t time_period_in_us)
//{
////	int32_t current_torque_data_sum = 0;
////	for (uint8_t i = 0; i < CURRENT_TORQUE_DATA_SIZE; i++)
////	{
////		current_torque_data_sum += g_motor_status._current_torque_data[i];
////	}
//	double l_time_period = time_period_in_us / 1000000.0;
//
//	// GOAL TORQUE
//	g_motor_command.goal_motor_torque_in_nm	= (((double) g_motor_command.goal_torque) / 32768.0) * MAX_READABLE_CURRENT * 0.1118;
//	g_motor_command.goal_joint_torque_in_nm	= g_motor_command.goal_motor_torque_in_nm * g_joint_configuration.gear_ratio;
//
//	// CURRENT TORQUE
////	g_motor_status.current_torque 				= (int16_t) (current_torque_data_sum / CURRENT_TORQUE_DATA_SIZE);
//	g_motor_status.current_motor_torque_in_nm	= (((double) g_motor_status.current_torque) / 32768.0) * MAX_READABLE_CURRENT * 0.1118;
//	g_motor_status.current_joint_torque_in_nm	= g_motor_status.current_motor_torque_in_nm * g_joint_configuration.gear_ratio;
//
//	// CURRENT SPEED
////	g_motor_status.current_hall_encoder_speed  		= g_motor_status.current_hall_encoder_position - g_motor_status.previous_hall_encoder_position;
////	double temp_speed = - 1.0 * (double) g_motor_status.current_hall_encoder_position;
//	double temp_speed = 0;
//	for (int i = 0; i < SPEED_DATA_SIZE; i++)
//	{
//		temp_speed += (double) g_motor_status._current_hall_speed_data[i];
//	}
//	g_motor_status.current_hall_encoder_speed		= temp_speed / SPEED_DATA_SIZE;
//
//	g_motor_status.current_motor_speed_in_rads 		= (g_motor_status.current_hall_encoder_speed * l_time_period) * HALL_IMPULSE_ANGLE_IN_RAD;
//	g_motor_status.current_motor_speed_in_rads_int  = (g_motor_status.current_motor_speed_in_rads * 32768.0);
//
//	g_motor_status.current_motor_speed_in_rpm  		= ((g_motor_status.current_hall_encoder_speed * time_period_in_us) / 84) * 60;
//
////	g_motor_status.current_joint_speed_in_rads 		= g_motor_status.current_motor_speed_in_rads / GEAR_RATIO;
////	g_motor_status.current_joint_speed_in_rpm  		= g_motor_status.current_motor_speed_in_rpm / GEAR_RATIO;
//}

//void RecalculateMotorState(void)
//{
////	int32_t current_torque_data_sum = 0;
////	for (uint8_t i = 0; i < CURRENT_TORQUE_DATA_SIZE; i++)
////	{
////		current_torque_data_sum += g_motor_status._current_torque_data[i];
////	}
//	// CURRENT POSITION
//	// GOAL TORQUE
//	g_motor_command.goal_motor_torque_in_nm	= (((double) g_motor_command.goal_torque) / 32768.0) * MAX_READABLE_CURRENT * 0.1118;
//	g_motor_command.goal_joint_torque_in_nm	= g_motor_command.goal_motor_torque_in_nm * GEAR_RATIO;
//
//	g_motor_status.current_joint_position_in_rad = M_TWOPI * ( (double) g_motor_status.current_hall_encoder_position / (double) ( g_joint_configuration.gear_ratio * g_joint_configuration.pole_pairs * 6) );
//
//	// CURRENT TORQUE
////	g_motor_status.current_torque 				= (int16_t) (current_torque_data_sum / CURRENT_TORQUE_DATA_SIZE);
//	g_motor_status.current_motor_torque_in_nm	= (((double) g_motor_status.current_torque) / 32768.0) * MAX_READABLE_CURRENT * 0.1118;
//	g_motor_status.current_joint_torque_in_nm	= g_motor_status.current_motor_torque_in_nm * GEAR_RATIO;
//
//	// CURRENT SPEED
////		int16_t g_previous_hall_encoder_position = 0;
//	g_motor_status.current_hall_encoder_speed  		= g_motor_status.current_hall_encoder_position - g_motor_status.previous_hall_encoder_position;
//	g_motor_status.current_motor_speed_in_rads 		= (g_motor_status.current_hall_encoder_speed * 100.0) * HALL_IMPULSE_ANGLEIN_RAD;
//	g_motor_status.current_motor_speed_in_rpm  		= ((g_motor_status.current_hall_encoder_speed * 100.0) / 84) * 60;
//	g_motor_status.current_joint_speed_in_rads 		= g_motor_status.current_motor_speed_in_rads / GEAR_RATIO;
//	g_motor_status.current_joint_speed_in_rpm  		= g_motor_status.current_motor_speed_in_rpm / GEAR_RATIO;
//	g_motor_status.previous_hall_encoder_position 	= g_motor_status.current_hall_encoder_position;
//}

#ifdef __cplusplus
}
#endif

#endif /* __APP_PARAMETERS_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
