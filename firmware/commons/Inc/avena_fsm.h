/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : fsm.h
  * @brief          : Header for fsm.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FSM_H
#define __FSM_H

#ifdef __cplusplus
extern "C" {
#endif

typedef enum FSM_State
{
	FSM_START = 0, /**< @brief Starting uC.*/
	FSM_INIT = 1,
	FSM_READY_TO_OPERATE = 2,
	FSM_OPERATION_ENABLE = 3, /**< @brief Enable power.*/

	FSM_TRANSITION_START_TO_INIT = 10,
	FSM_TRANSITION_INIT_TO_READY_TO_OPERATE = 11,
	FSM_TRANSITION_READY_TO_OPERATE_TO_OPERATION_ENABLE = 12,
	FSM_TRANSITION_OPERATION_ENABLE_TO_READY_TO_OPERATE = 13,
	FSM_TRANSITION_FAULT_REACTION_ACTIVE_TO_FAULT = 14,
	FSM_TRANSITION_FAULT_TO_READY_TO_OPERATE = 15,

	FSM_FAULT_REACTION_ACTIVE = 254,
	FSM_FAULT = 255
} FSM_State_t;

void FSM_Transition10Callback(void);
void FSM_Transition11Callback(void);
void FSM_Transition12Callback(void);
void FSM_Transition13Callback(void);
void FSM_Transition14Callback(void);
void FSM_Transition15Callback(void);

void FSM_StateStartCallback(void);
void FSM_StateInitCallback(void);
void FSM_StateReadyToOperateCallback(void);
void FSM_StateOperationEnableCallback(void);
void FSM_StateFaultReactionActiveCallback(void);
void FSM_StateFaultCallback(void);

void FSM_Tick(void);

FSM_State_t FSM_GetState(void);

void FSM_Transition10(void);
void FSM_Transition11(void);
void FSM_Transition12(void);
void FSM_Transition13(void);
void FSM_Transition14(void);
void FSM_Transition15(void);

/* Includes ------------------------------------------------------------------*/

/* Private includes ----------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/

/* Exported functions prototypes ---------------------------------------------*/

/* Private defines -----------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif /* __FSM_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
