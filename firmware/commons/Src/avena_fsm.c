/*
 * fsm.c
 *
 *  Created on: Jul 10, 2021
 *      Author: lukasz.lecki@pomagier.info
 */

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "avena_fsm.h"

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/
//#define THERMISTORNOMINAL 10000

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
volatile FSM_State_t g_fsm_current_state = FSM_START;

/* Private function prototypes -----------------------------------------------*/

/* Private user code ---------------------------------------------------------*/
void FSM_Tick(void)
{
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wswitch"
	switch (g_fsm_current_state) {

		case FSM_START:
			FSM_StateStartCallback();

			break;

		case FSM_INIT:
			FSM_StateInitCallback();

			// Automatic transition
			// FSM_Transition11();
			break;

		case FSM_READY_TO_OPERATE:
			FSM_StateReadyToOperateCallback();
			break;

		case FSM_OPERATION_ENABLE:
			FSM_StateOperationEnableCallback();
			break;

		case FSM_FAULT_REACTION_ACTIVE:
			FSM_StateFaultReactionActiveCallback();

			// Automatic transition
			FSM_Transition14();
			break;

		case FSM_FAULT:
			FSM_StateFaultCallback();
			break;
	}

#pragma GCC diagnostic pop
}

FSM_State_t FSM_GetState(void)
{
	return g_fsm_current_state;
}

void __attribute__((weak)) FSM_StateStartCallback(void)
{

}

void __attribute__((weak)) FSM_StateInitCallback(void)
{

}

void __attribute__((weak)) FSM_StateReadyToOperateCallback(void)
{

}

void __attribute__((weak)) FSM_StateOperationEnableCallback(void)
{

}

void __attribute__((weak)) FSM_StateFaultReactionActiveCallback(void)
{

}

void __attribute__((weak)) FSM_StateFaultCallback(void)
{

}

void __attribute__((weak)) FSM_Transition10Callback(void)
{

}

void __attribute__((weak)) FSM_Transition11Callback(void)
{

}

void __attribute__((weak)) FSM_Transition12Callback(void)
{

}

void __attribute__((weak)) FSM_Transition13Callback(void)
{

}

void __attribute__((weak)) FSM_Transition14Callback(void)
{

}

void __attribute__((weak)) FSM_Transition15Callback(void)
{

}

void FSM_Transition10(void)
{
	if (g_fsm_current_state != FSM_START) return;

	g_fsm_current_state = FSM_TRANSITION_START_TO_INIT;

	FSM_Transition10Callback();

	// END OF TRANSITION
	g_fsm_current_state = FSM_INIT;
}

void FSM_Transition11(void)
{
	if (g_fsm_current_state != FSM_INIT) return;

	g_fsm_current_state = FSM_TRANSITION_INIT_TO_READY_TO_OPERATE;

	FSM_Transition11Callback();

	// END OF TRANSITION
	g_fsm_current_state = FSM_READY_TO_OPERATE;
}

void FSM_Transition12(void)
{
	if (g_fsm_current_state != FSM_READY_TO_OPERATE) return;

	g_fsm_current_state = FSM_TRANSITION_READY_TO_OPERATE_TO_OPERATION_ENABLE;

	FSM_Transition12Callback();

	// END OF TRANSITION
	g_fsm_current_state = FSM_OPERATION_ENABLE;
}

void FSM_Transition13(void)
{
	if (g_fsm_current_state != FSM_OPERATION_ENABLE) return;

	g_fsm_current_state = FSM_TRANSITION_OPERATION_ENABLE_TO_READY_TO_OPERATE;

	FSM_Transition13Callback();

	// END OF TRANSITION
	g_fsm_current_state = FSM_READY_TO_OPERATE;
}

void FSM_Transition14(void)
{
	g_fsm_current_state = FSM_FAULT_REACTION_ACTIVE;

	FSM_Transition14Callback();

	// END OF TRANSITION
	g_fsm_current_state = FSM_FAULT;
}

void FSM_Transition15(void)
{
	if (g_fsm_current_state != FSM_FAULT) return;

	g_fsm_current_state = FSM_TRANSITION_FAULT_TO_READY_TO_OPERATE;

	FSM_Transition15Callback();

	// END OF TRANSITION
	g_fsm_current_state = FSM_INIT;
}


// FSM Transitions
//void FSM_Transition0(void)
//{
//	if (g_fsm_current_state != FSM_START) return;
//
//	// Configure CAN
//	const uint8_t _can_start_address = 10;
//
//	uint8_t _dip1 = (HAL_GPIO_ReadPin(GPIOB, DIP1_Pin) == GPIO_PIN_RESET) ? (8) : (0);
//	uint8_t _dip2 = (HAL_GPIO_ReadPin(GPIOB, DIP2_Pin) == GPIO_PIN_RESET) ? (4) : (0);
//	uint8_t _dip3 = (HAL_GPIO_ReadPin(GPIOB, DIP3_Pin) == GPIO_PIN_RESET) ? (2) : (0);
//	uint8_t _dip4 = (HAL_GPIO_ReadPin(GPIOB, DIP4_Pin) == GPIO_PIN_RESET) ? (1) : (0);
//	g_joint_id = _dip1 + _dip2 + _dip3 + _dip4;
//
//	// CAN FILTER
//	g_can_filter_config.IdType = FDCAN_STANDARD_ID;
//	g_can_filter_config.FilterIndex = 0;
//	g_can_filter_config.FilterType = FDCAN_FILTER_DUAL;
//	g_can_filter_config.FilterConfig = FDCAN_FILTER_TO_RXFIFO0;
//
//	g_can_filter_config.FilterID1 =  g_joint_id + _can_start_address;
//	g_can_filter_config.FilterID2 = 0x0AA ;
//
//  	// CAN TRANSMITION CONFIGURATION
//	g_can_tx_header.Identifier = (g_joint_id + _can_start_address) * 16;
//	g_can_tx_header.IdType = FDCAN_STANDARD_ID;
//	g_can_tx_header.TxFrameType = FDCAN_DATA_FRAME;
//	g_can_tx_header.DataLength = FDCAN_DLC_BYTES_6;
//	g_can_tx_header.ErrorStateIndicator = FDCAN_ESI_ACTIVE;
//	g_can_tx_header.BitRateSwitch = FDCAN_BRS_ON;
//	g_can_tx_header.FDFormat = FDCAN_FD_CAN;
//	g_can_tx_header.TxEventFifoControl = FDCAN_NO_TX_EVENTS;
//	g_can_tx_header.MessageMarker = 0;
//
//	HAL_FDCAN_ConfigTxDelayCompensation(&hfdcan1, 10, 0);
//	HAL_FDCAN_EnableTxDelayCompensation(&hfdcan1);
//
//	HAL_FDCAN_ConfigFilter(&hfdcan1, &g_can_filter_config); //Initialize CAN Filter
//	HAL_FDCAN_ConfigGlobalFilter(&hfdcan1, 3, 3, FDCAN_FILTER_REMOTE, FDCAN_REJECT_REMOTE);
//	HAL_FDCAN_Start(&hfdcan1); //Initialize CAN Bus
//	HAL_FDCAN_ActivateNotification(&hfdcan1, FDCAN_IT_RX_FIFO0_NEW_MESSAGE, 0);// Initialize CAN Bus Rx Interrupt
//	HAL_FDCAN_EnableISOMode(&hfdcan1);
//
//	// END OF TRANSITION
//	g_fsm_current_state = FSM_NOT_READY_TO_SWITCH_ON;
//}
//
//
//void FSM_Transition1(void)
//{
//	if (g_fsm_current_state != FSM_NOT_READY_TO_SWITCH_ON) return;
//	// Calculation position from absolute encoder
//
//	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_SET); // Enable Gate driver
//
//	// END OF TRANSITION
//	g_fsm_current_state = FSM_SWITCH_ON_DISABLED;
//}
//
//void FSM_Transition2(void)
//{
//	if (g_fsm_current_state != FSM_SWITCH_ON_DISABLED) return;
//
//	// END OF TRANSITION
//	g_fsm_current_state = FSM_READY_TO_SWITCH_ON;
//}
//
//void FSM_Transition3(void)
//{
//	if (g_fsm_current_state != FSM_READY_TO_SWITCH_ON) return;
//
//	// END OF TRANSITION
//	g_fsm_current_state = FSM_SWITCHED_ON;
//}
//
//void FSM_Transition4(void)
//{
//	if (g_fsm_current_state != FSM_SWITCHED_ON) return;
//
//	// END OF TRANSITION
//	g_fsm_current_state = FSM_OPERATION_ENABLE;
//}
//
//void FSM_Transition5(void)
//{
//	if (g_fsm_current_state != FSM_OPERATION_ENABLE) return;
//
//	// END OF TRANSITION
//	g_fsm_current_state = FSM_SWITCHED_ON;
//}
//
//void FSM_Transition6(void)
//{
//	if (g_fsm_current_state != FSM_SWITCHED_ON) return;
//
//	// END OF TRANSITION
//	g_fsm_current_state = FSM_READY_TO_SWITCH_ON;
//}
//
//void FSM_Transition7(void)
//{
//	if (g_fsm_current_state != FSM_READY_TO_SWITCH_ON) return;
//
//	// END OF TRANSITION
//	g_fsm_current_state = FSM_SWITCH_ON_DISABLED;
//}

//void FSM_Transition8(void)
//{
//	if (g_fsm_current_state != FSM_STATE_SWITCHED_ON) return;
//
//	// END OF TRANSITION
//	g_fsm_current_state = FSM_STATE_OPERATION_ENABLE;
//}

//void FSM_Transition9(void)
//{
//	if (g_fsm_current_state != FSM_STATE_SWITCHED_ON) return;
//
//	// END OF TRANSITION
//	g_fsm_current_state = FSM_STATE_OPERATION_ENABLE;
//}

//void FSM_Transition10(void)
//{
//	if (g_fsm_current_state != FSM_STATE_SWITCHED_ON) return;
//
//	// END OF TRANSITION
//	g_fsm_current_state = FSM_STATE_OPERATION_ENABLE;
//}

//void FSM_Transition13(void)
//{
//
//	// END OF TRANSITION
//	g_fsm_current_state = FSM_FAULT_REACTION_ACTIVE;
//}
//
//void FSM_Transition14(void)
//{
//	MC_StopMotor1();
//
//	// END OF TRANSITION
//	g_fsm_current_state = FSM_FAULT;
//}

//void FSM_Transition15(void)
//{
//	if (g_fsm_current_state != FSM_FAULT) return;
//
//	// END OF TRANSITION
//	g_fsm_current_state = FSM_OPERATION_ENABLE;
//}


