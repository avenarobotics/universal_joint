################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/Users/lukas/Desktop/ST/universal_joint/firmware/Commons/Src/avena_fsm.c 

OBJS += \
./Commons/Src/avena_fsm.o 

C_DEPS += \
./Commons/Src/avena_fsm.d 


# Each subdirectory must supply rules for building sources it contributes
Commons/Src/avena_fsm.o: C:/Users/lukas/Desktop/ST/universal_joint/firmware/Commons/Src/avena_fsm.c Commons/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DARM_MATH_CM4 -DUSE_HAL_DRIVER -DSTM32G431xx -c -I../../Inc -I../../Drivers/STM32G4xx_HAL_Driver/Inc -I../../Drivers/STM32G4xx_HAL_Driver/Inc/Legacy -I../../MCSDK_v5.4.6/MotorControl/MCSDK/MCLib/Any/Inc -I../../MCSDK_v5.4.6/MotorControl/MCSDK/MCLib/G4xx/Inc -I../../MCSDK_v5.4.6/MotorControl/MCSDK/UILibrary/Inc -I../../MCSDK_v5.4.6/MotorControl/MCSDK/SystemDriveParams -I../../Drivers/CMSIS/Device/ST/STM32G4xx/Include -I../../Drivers/CMSIS/Include -I../../Drivers/CMSIS/DSP/Include -I../../../Commons/Inc -Ofast -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

