/*
 * FeeTechRS485.h
 *
 *  Created on: 29 mar 2021
 *      Author: John
 */

#ifndef FEETECHRS485_H_
#define FEETECHRS485_H_

#define SendPing 1
#define SendMove 2
#define SendPosition 3
#define SendTemp 4
#define TransferComplete 0


int MovePosition(unsigned char ID, signed short int position, unsigned short int velocity);
int Ping(unsigned char ID);
int AskPosition(unsigned char ID);
int AskTemp(unsigned char ID);



#endif /* FEETECHRS485_H_ */
