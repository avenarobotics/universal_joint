/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <avena_fsm.h>
#include <avena_parameters.h>
#include <app_parameters.h>
#include <math.h>
#include <stdlib.h>

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
ADC_HandleTypeDef hadc2;

CORDIC_HandleTypeDef hcordic;

FDCAN_HandleTypeDef hfdcan1;

SPI_HandleTypeDef hspi2;
DMA_HandleTypeDef hdma_spi2_rx;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim6;
TIM_HandleTypeDef htim7;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */

// COUNTERS
volatile uint16_t g_can_rx_counter 	= 0;
volatile uint16_t g_can_tx_counter 	= 0;
volatile uint32_t g_counter_10000hz = 0;
volatile uint32_t g_counter_1000hz 	= 0;
volatile uint32_t g_counter_100hz 	= 0;
volatile uint32_t g_counter_10hz 	= 0;
volatile uint32_t g_counter_1hz 	= 0;
volatile uint16_t g_timer_counter 	= 0;

bool g_motor_reach_torque_limit = false;
bool g_motor_started 			= false;

const double  speed_limit  		= 0.01;
int16_t starting_torque 		= 100;
int16_t increase_torque_step	= 1;
int16_t decrease_torque_step	= 10;
int16_t torque_limit 			= 2000;

// ERRORS
NodeStatus_t 	g_node_status 		=
{
	.can_node_id = 0,
};

MotorCommand_t 	g_motor_command 	=
{
	.goal_torque = 0,
};

JointConfiguration_t 	g_joint_configuration 	=
{
	.calibration_state = JOINT_NOT_CALIBRATED,
	.working_area_constrain = true,
	.ma730_exists = true,
};

MotorStatus_t 	g_motor_status =
{
	.encoder_position_state = POSITION_UNKNOWN,
};

FDCAN_FilterTypeDef   g_can_filter_config; //CAN Bus Filter
FDCAN_RxHeaderTypeDef g_can_rx_header; // CAN Bus Transmit Header
FDCAN_TxHeaderTypeDef g_can_tx_header; // CAN Bus Receive Header
uint8_t g_can_rx_data[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};  //CAN Bus Receive Buffer
uint8_t g_can_tx_data[12] = {0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70, 0x80, 0x90, 0xA0, 0xB0, 0xC0};
HAL_StatusTypeDef g_can_tx_status;

int16_t  g_MA730_read_buffer 	= 0;

volatile int16_t g_zero_sector_number = -1;
volatile int16_t g_current_sector_number = -1;
volatile int16_t g_previous_sector_number = -1;
volatile int16_t g_previous_selected_electric_rotation = -1;
volatile int16_t g_current_selected_electric_rotation = -1;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_ADC1_Init(void);
static void MX_ADC2_Init(void);
static void MX_CORDIC_Init(void);
static void MX_TIM1_Init(void);
static void MX_TIM2_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_TIM6_Init(void);
static void MX_TIM7_Init(void);
static void MX_FDCAN1_Init(void);
static void MX_DMA_Init(void);
static void MX_SPI2_Init(void);
static void MX_NVIC_Init(void);
/* USER CODE BEGIN PFP */
uint32_t Flash_Write_Data (uint32_t StartPageAddress, uint32_t *Data, uint16_t numberofwords);
void Flash_Read_Data (uint32_t StartPageAddress, uint32_t *RxBuf, uint16_t numberofwords);
void motor_start_cw(int16_t speed_limit);
void motor_start_ccw(int16_t speed_limit);
void motor_speed_up_cw(int16_t speed_limit, int16_t increase_torque_step);
void motor_slow_down_cw(int16_t speed_limit, int16_t decrese_torque_step);
void motor_speed_up_ccw(int16_t speed_limit, int16_t increase_torque_step);
void motor_slow_down_ccw(int16_t speed_limit, int16_t decrese_torque_step);
void motor_stop();
bool motor_in_position(int16_t position);
uint16_t read_ma730_value();
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */
//  g_motor_status.current_temperature_double = 0;

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ADC1_Init();
  MX_ADC2_Init();
  MX_CORDIC_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_USART2_UART_Init();
  MX_MotorControl_Init();
  MX_TIM6_Init();
  MX_TIM7_Init();
  MX_FDCAN1_Init();
  MX_DMA_Init();
  MX_SPI2_Init();

  /* Initialize interrupts */
  MX_NVIC_Init();
  /* USER CODE BEGIN 2 */
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  FSM_Tick(); // First tick - transition0 starting timers

  HAL_Delay(100); // Wait for stable MA730 readings

  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Configure the main internal regulator output voltage
  */
  HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1_BOOST);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV4;
  RCC_OscInitStruct.PLL.PLLN = 80;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV8;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the peripherals clocks
  */
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_ADC12
                              |RCC_PERIPHCLK_FDCAN;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.FdcanClockSelection = RCC_FDCANCLKSOURCE_PCLK1;
  PeriphClkInit.Adc12ClockSelection = RCC_ADC12CLKSOURCE_PLL;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief NVIC Configuration.
  * @retval None
  */
static void MX_NVIC_Init(void)
{
  /* TIM1_BRK_TIM15_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM1_BRK_TIM15_IRQn, 4, 1);
  HAL_NVIC_EnableIRQ(TIM1_BRK_TIM15_IRQn);
  /* TIM1_UP_TIM16_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM1_UP_TIM16_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(TIM1_UP_TIM16_IRQn);
  /* ADC1_2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(ADC1_2_IRQn, 2, 0);
  HAL_NVIC_EnableIRQ(ADC1_2_IRQn);
  /* TIM2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM2_IRQn, 3, 0);
  HAL_NVIC_EnableIRQ(TIM2_IRQn);
  /* USART2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(USART2_IRQn, 3, 1);
  HAL_NVIC_EnableIRQ(USART2_IRQn);
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_MultiModeTypeDef multimode = {0};
  ADC_InjectionConfTypeDef sConfigInjected = {0};
  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Common config
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV4;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.DataAlign = ADC_DATAALIGN_LEFT;
  hadc1.Init.GainCompensation = 0;
  hadc1.Init.ScanConvMode = ADC_SCAN_ENABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc1.Init.LowPowerAutoWait = DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  hadc1.Init.OversamplingMode = DISABLE;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the ADC multi-mode
  */
  multimode.Mode = ADC_MODE_INDEPENDENT;
  if (HAL_ADCEx_MultiModeConfigChannel(&hadc1, &multimode) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Injected Channel
  */
  sConfigInjected.InjectedChannel = ADC_CHANNEL_2;
  sConfigInjected.InjectedRank = ADC_INJECTED_RANK_1;
  sConfigInjected.InjectedSamplingTime = ADC_SAMPLETIME_2CYCLES_5;
  sConfigInjected.InjectedSingleDiff = ADC_SINGLE_ENDED;
  sConfigInjected.InjectedOffsetNumber = ADC_OFFSET_NONE;
  sConfigInjected.InjectedOffset = 0;
  sConfigInjected.InjectedNbrOfConversion = 2;
  sConfigInjected.InjectedDiscontinuousConvMode = DISABLE;
  sConfigInjected.AutoInjectedConv = DISABLE;
  sConfigInjected.QueueInjectedContext = DISABLE;
  sConfigInjected.ExternalTrigInjecConv = ADC_EXTERNALTRIGINJEC_T1_TRGO;
  sConfigInjected.ExternalTrigInjecConvEdge = ADC_EXTERNALTRIGINJECCONV_EDGE_RISING;
  sConfigInjected.InjecOversamplingMode = DISABLE;
  if (HAL_ADCEx_InjectedConfigChannel(&hadc1, &sConfigInjected) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Injected Channel
  */
  sConfigInjected.InjectedChannel = ADC_CHANNEL_14;
  sConfigInjected.InjectedRank = ADC_INJECTED_RANK_2;
  if (HAL_ADCEx_InjectedConfigChannel(&hadc1, &sConfigInjected) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_11;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_47CYCLES_5;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief ADC2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC2_Init(void)
{

  /* USER CODE BEGIN ADC2_Init 0 */

  /* USER CODE END ADC2_Init 0 */

  ADC_InjectionConfTypeDef sConfigInjected = {0};
  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC2_Init 1 */

  /* USER CODE END ADC2_Init 1 */
  /** Common config
  */
  hadc2.Instance = ADC2;
  hadc2.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV4;
  hadc2.Init.Resolution = ADC_RESOLUTION_12B;
  hadc2.Init.DataAlign = ADC_DATAALIGN_LEFT;
  hadc2.Init.GainCompensation = 0;
  hadc2.Init.ScanConvMode = ADC_SCAN_ENABLE;
  hadc2.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc2.Init.LowPowerAutoWait = DISABLE;
  hadc2.Init.ContinuousConvMode = DISABLE;
  hadc2.Init.NbrOfConversion = 1;
  hadc2.Init.DiscontinuousConvMode = DISABLE;
  hadc2.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc2.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc2.Init.DMAContinuousRequests = DISABLE;
  hadc2.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  hadc2.Init.OversamplingMode = DISABLE;
  if (HAL_ADC_Init(&hadc2) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Injected Channel
  */
  sConfigInjected.InjectedChannel = ADC_CHANNEL_14;
  sConfigInjected.InjectedRank = ADC_INJECTED_RANK_1;
  sConfigInjected.InjectedSamplingTime = ADC_SAMPLETIME_2CYCLES_5;
  sConfigInjected.InjectedSingleDiff = ADC_SINGLE_ENDED;
  sConfigInjected.InjectedOffsetNumber = ADC_OFFSET_NONE;
  sConfigInjected.InjectedOffset = 0;
  sConfigInjected.InjectedNbrOfConversion = 2;
  sConfigInjected.InjectedDiscontinuousConvMode = DISABLE;
  sConfigInjected.AutoInjectedConv = DISABLE;
  sConfigInjected.QueueInjectedContext = DISABLE;
  sConfigInjected.ExternalTrigInjecConv = ADC_EXTERNALTRIGINJEC_T1_TRGO;
  sConfigInjected.ExternalTrigInjecConvEdge = ADC_EXTERNALTRIGINJECCONV_EDGE_RISING;
  sConfigInjected.InjecOversamplingMode = DISABLE;
  if (HAL_ADCEx_InjectedConfigChannel(&hadc2, &sConfigInjected) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Injected Channel
  */
  sConfigInjected.InjectedChannel = ADC_CHANNEL_3;
  sConfigInjected.InjectedRank = ADC_INJECTED_RANK_2;
  if (HAL_ADCEx_InjectedConfigChannel(&hadc2, &sConfigInjected) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_1;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_47CYCLES_5;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  if (HAL_ADC_ConfigChannel(&hadc2, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC2_Init 2 */

  /* USER CODE END ADC2_Init 2 */

}

/**
  * @brief CORDIC Initialization Function
  * @param None
  * @retval None
  */
static void MX_CORDIC_Init(void)
{

  /* USER CODE BEGIN CORDIC_Init 0 */

  /* USER CODE END CORDIC_Init 0 */

  /* USER CODE BEGIN CORDIC_Init 1 */

  /* USER CODE END CORDIC_Init 1 */
  hcordic.Instance = CORDIC;
  if (HAL_CORDIC_Init(&hcordic) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN CORDIC_Init 2 */

  /* USER CODE END CORDIC_Init 2 */

}

/**
  * @brief FDCAN1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_FDCAN1_Init(void)
{

  /* USER CODE BEGIN FDCAN1_Init 0 */

  /* USER CODE END FDCAN1_Init 0 */

  /* USER CODE BEGIN FDCAN1_Init 1 */

  /* USER CODE END FDCAN1_Init 1 */
  hfdcan1.Instance = FDCAN1;
  hfdcan1.Init.ClockDivider = FDCAN_CLOCK_DIV2;
  hfdcan1.Init.FrameFormat = FDCAN_FRAME_FD_BRS;
  hfdcan1.Init.Mode = FDCAN_MODE_NORMAL;
  hfdcan1.Init.AutoRetransmission = ENABLE;
  hfdcan1.Init.TransmitPause = ENABLE;
  hfdcan1.Init.ProtocolException = DISABLE;
  hfdcan1.Init.NominalPrescaler = 5;
  hfdcan1.Init.NominalSyncJumpWidth = 3;
  hfdcan1.Init.NominalTimeSeg1 = 12;
  hfdcan1.Init.NominalTimeSeg2 = 3;
  hfdcan1.Init.DataPrescaler = 1;
  hfdcan1.Init.DataSyncJumpWidth = 3;
  hfdcan1.Init.DataTimeSeg1 = 12;
  hfdcan1.Init.DataTimeSeg2 = 3;
  hfdcan1.Init.StdFiltersNbr = 1;
  hfdcan1.Init.ExtFiltersNbr = 0;
  hfdcan1.Init.TxFifoQueueMode = FDCAN_TX_FIFO_OPERATION;
  if (HAL_FDCAN_Init(&hfdcan1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN FDCAN1_Init 2 */

  /* USER CODE END FDCAN1_Init 2 */

}

/**
  * @brief SPI2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI2_Init(void)
{

  /* USER CODE BEGIN SPI2_Init 0 */

  /* USER CODE END SPI2_Init 0 */

  /* USER CODE BEGIN SPI2_Init 1 */

  /* USER CODE END SPI2_Init 1 */
  /* SPI2 parameter configuration*/
  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_16BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi2.Init.NSS = SPI_NSS_SOFT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_16;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 7;
  hspi2.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi2.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI2_Init 2 */

  /* USER CODE END SPI2_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_SlaveConfigTypeDef sSlaveConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = ((TIM_CLOCK_DIVIDER) - 1);
  htim1.Init.CounterMode = TIM_COUNTERMODE_CENTERALIGNED1;
  htim1.Init.Period = ((PWM_PERIOD_CYCLES) / 2);
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV2;
  htim1.Init.RepetitionCounter = (REP_COUNTER);
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sSlaveConfig.SlaveMode = TIM_SLAVEMODE_TRIGGER;
  sSlaveConfig.InputTrigger = TIM_TS_ITR1;
  if (HAL_TIM_SlaveConfigSynchro(&htim1, &sSlaveConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_OC4REF;
  sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_SET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM2;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_ENABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_ENABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_1;
  sBreakDeadTimeConfig.DeadTime = ((DEAD_TIME_COUNTS) / 2);
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_LOW;
  sBreakDeadTimeConfig.BreakFilter = 0;
  sBreakDeadTimeConfig.BreakAFMode = TIM_BREAK_AFMODE_INPUT;
  sBreakDeadTimeConfig.Break2State = TIM_BREAK2_DISABLE;
  sBreakDeadTimeConfig.Break2Polarity = TIM_BREAK2POLARITY_HIGH;
  sBreakDeadTimeConfig.Break2Filter = 0;
  sBreakDeadTimeConfig.Break2AFMode = TIM_BREAK_AFMODE_INPUT;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */
  HAL_TIM_MspPostInit(&htim1);

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_HallSensor_InitTypeDef sConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 0;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = M1_HALL_TIM_PERIOD;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfig.IC1Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC1Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC1Filter = M1_HALL_IC_FILTER;
  sConfig.Commutation_Delay = 0;
  if (HAL_TIMEx_HallSensor_Init(&htim2, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_OC2REF;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief TIM6 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM6_Init(void)
{

  /* USER CODE BEGIN TIM6_Init 0 */

  /* USER CODE END TIM6_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM6_Init 1 */

  /* USER CODE END TIM6_Init 1 */
  htim6.Instance = TIM6;
  htim6.Init.Prescaler = 40000;
  htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim6.Init.Period = 39;
  htim6.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim6, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM6_Init 2 */

  /* USER CODE END TIM6_Init 2 */

}

/**
  * @brief TIM7 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM7_Init(void)
{

  /* USER CODE BEGIN TIM7_Init 0 */

  /* USER CODE END TIM7_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM7_Init 1 */

  /* USER CODE END TIM7_Init 1 */
  htim7.Instance = TIM7;
  htim7.Init.Prescaler = 0;
  htim7.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim7.Init.Period = 15999;
  htim7.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim7) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim7, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM7_Init 2 */

  /* USER CODE END TIM7_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.Init.ClockPrescaler = UART_PRESCALER_DIV1;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetTxFifoThreshold(&huart2, UART_TXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetRxFifoThreshold(&huart2, UART_RXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_DisableFifoMode(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMAMUX1_CLK_ENABLE();
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, BRAKE_RESISTOR_ENABLE_Pin|DRV8323_ENABLE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(MA730_CS_GPIO_Port, MA730_CS_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : BRAKE_RESISTOR_ENABLE_Pin DRV8323_ENABLE_Pin */
  GPIO_InitStruct.Pin = BRAKE_RESISTOR_ENABLE_Pin|DRV8323_ENABLE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : MA730_CS_Pin */
  GPIO_InitStruct.Pin = MA730_CS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(MA730_CS_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : DIP1_Pin DIP2_Pin DIP3_Pin DIP4_Pin */
  GPIO_InitStruct.Pin = DIP1_Pin|DIP2_Pin|DIP3_Pin|DIP4_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
void HAL_FDCAN_RxFifo0Callback(FDCAN_HandleTypeDef *hfdcan1, uint32_t RxFifo0ITs)
{

	if((RxFifo0ITs & FDCAN_IT_RX_FIFO0_NEW_MESSAGE) != 0)
	{

		// RETRIEVE CAN MESSAGE -------------------------------------------------------------------------
		HAL_FDCAN_GetRxMessage(hfdcan1, FDCAN_RX_FIFO0, &g_can_rx_header, g_can_rx_data);
		g_can_rx_counter++;

		// RESET SAFE TIMER -----------------------------------------------------------------------------
		HAL_TIM_Base_Stop(&htim6);
		TIM6->CNT = 0;
		HAL_TIM_Base_Start_IT(&htim6);

		// UPDATE JOINT INFO ----------------------------------------------------------------------------
//		RecalculateJointState();

//		uint8_t l_can_cmd_motor_run = 0;

		if (g_can_rx_header.Identifier == 0x0AA)
		{
			g_motor_command.goal_torque  = g_can_rx_data[g_node_status.can_node_id * 3] << 8;
			g_motor_command.goal_torque += g_can_rx_data[g_node_status.can_node_id * 3 + 1];
			uint8_t temp_new_fsm_state   = g_can_rx_data[g_node_status.can_node_id * 3 + 2];

			if (FSM_GetState() != temp_new_fsm_state) {
				switch (temp_new_fsm_state) {

					case FSM_INIT:

						switch (FSM_GetState()) {
							case FSM_FAULT:
								FSM_Transition15();
								break;

							case FSM_START:
								FSM_Transition10();
								break;

							case FSM_INIT:
							case FSM_READY_TO_OPERATE:
							case FSM_OPERATION_ENABLE:
							case FSM_TRANSITION_START_TO_INIT:
							case FSM_TRANSITION_INIT_TO_READY_TO_OPERATE:
							case FSM_TRANSITION_READY_TO_OPERATE_TO_OPERATION_ENABLE:
							case FSM_TRANSITION_OPERATION_ENABLE_TO_READY_TO_OPERATE:
							case FSM_TRANSITION_FAULT_REACTION_ACTIVE_TO_FAULT:
							case FSM_TRANSITION_FAULT_TO_READY_TO_OPERATE:
							case FSM_FAULT_REACTION_ACTIVE:
								break;
						}

						break;

					case FSM_READY_TO_OPERATE:
						FSM_Transition13();
						break;

					case FSM_OPERATION_ENABLE:
						FSM_Transition12();
						break;

				}
			}

			int16_t l_current_torque = g_motor_status.current_torque;
			if (g_motor_command.goal_torque < 0)
			{
				l_current_torque = -l_current_torque;
			}

			g_can_tx_data[0] 	= g_motor_status.current_hall_encoder_position >> 8;
			g_can_tx_data[1] 	= g_motor_status.current_hall_encoder_position;
			g_can_tx_data[2] 	= g_motor_status.current_joint_speed_in_rads_int >> 8;
			g_can_tx_data[3] 	= g_motor_status.current_joint_speed_in_rads_int;
			g_can_tx_data[4] 	= l_current_torque >> 8;
			g_can_tx_data[5] 	= l_current_torque;
			g_can_tx_data[6] 	= g_motor_status.current_temperature;
			g_can_tx_data[7] 	= FSM_GetState();
			g_can_tx_data[8] 	= g_motor_status.mc_current_faults_motor;
			g_can_tx_data[9] 	= g_motor_status.mc_occured_faults_motor;
			g_can_tx_data[10]	= g_motor_status.errors;
			g_can_tx_data[11]	= g_motor_status.warnings;

			// SEND FRAME VIA FD CAN -------------------------------------------------------------------------------
			if (HAL_FDCAN_AddMessageToTxFifoQ(hfdcan1, &g_can_tx_header, g_can_tx_data) == HAL_OK)
			{
				g_can_tx_counter++;
			}
		}
	}
}

uint16_t read_ma730_value() {

	// READ MA730 Position
	HAL_GPIO_WritePin(MA730_CS_GPIO_Port, MA730_CS_Pin, GPIO_PIN_RESET);

	HAL_SPI_Receive(&hspi2, (uint8_t * ) &g_MA730_read_buffer, 16, 100);
	g_MA730_read_buffer = (g_MA730_read_buffer >> 2) & 0b0011111111111111;

//	HAL_SPI_Receive(&hspi2, (uint8_t * ) &g_MA730_read_buffer, 16, 100);
//	g_MA730_read_buffer = (g_MA730_read_buffer >> 2) & 0b0011111111111111;

	HAL_GPIO_WritePin(MA730_CS_GPIO_Port, MA730_CS_Pin, GPIO_PIN_SET);

	return g_MA730_read_buffer;
}

void motor_start_cw(int16_t _speed_limit) {
  if (!g_motor_started)
    {
//      g_current_speed_limit = _speed_limit;
      g_motor_command.goal_torque = starting_torque;
//      g_current_max_speed = 0;
      MC_ProgramTorqueRampMotor1(g_motor_command.goal_torque, 1);
      MC_StartMotor1();
//      HAL_Delay(10);
    }
  g_motor_started = true;
}

void motor_start_ccw(int16_t _speed_limit)
{
  if (!g_motor_started)
    {
      g_motor_command.goal_torque = -1 * starting_torque;
      MC_ProgramTorqueRampMotor1(g_motor_command.goal_torque, 1);
      MC_StartMotor1();
    }
  g_motor_started = true;
}

void motor_speed_up_cw(int16_t speed_limit, int16_t increase_torque_step)
{

  if (abs(g_motor_status.current_torque) > torque_limit || abs(g_motor_command.goal_torque) > torque_limit)
    {
      g_motor_reach_torque_limit = true;
    }
  else
    {
      g_motor_reach_torque_limit = false;
    }

    if (!g_motor_reach_torque_limit) {
	  g_motor_command.goal_torque += increase_torque_step;

      MC_ProgramTorqueRampMotor1(g_motor_command.goal_torque, 1);
      MC_StartMotor1();
    }
}

void motor_slow_down_cw(int16_t speed_limit, int16_t decrease_torque_step)
{
	if (g_motor_command.goal_torque - decrease_torque_step > starting_torque) {
		g_motor_command.goal_torque -= decrease_torque_step;
	}
    MC_ProgramTorqueRampMotor1(g_motor_command.goal_torque, 1);
    MC_StartMotor1();
}

void motor_speed_up_ccw(int16_t speed_limit, int16_t increase_torque_step)
{
//  bool speed_is_changed_ = false;

//  if (abs(g_motor_status.current_hall_encoder_speed) <= g_current_speed_limit)
//    {
//      g_motor_reach_speed_limit = false;
//    }
//  else
//    {
//      g_motor_reach_speed_limit = true;
//    }

  if (abs(g_motor_status.current_torque) > torque_limit || abs(g_motor_command.goal_torque) > torque_limit)
    {
      g_motor_reach_torque_limit = true;
    }
  else
    {
      g_motor_reach_torque_limit = false;
    }

//  if (!g_motor_reach_speed_limit && !g_motor_reach_torque_limit) {
    if (!g_motor_reach_torque_limit) {
	  g_motor_command.goal_torque -= increase_torque_step;
//      speed_is_changed_ = true;
//  }
//
//  if (speed_is_changed_)
//    {
      MC_ProgramTorqueRampMotor1(g_motor_command.goal_torque, 1);
      MC_StartMotor1();
//      HAL_Delay(1);
    }
}

void motor_slow_down_ccw(int16_t speed_limit, int16_t decrease_torque_step)
{
	if (g_motor_command.goal_torque + decrease_torque_step < -1 * starting_torque) {
		g_motor_command.goal_torque += decrease_torque_step;

	}
    MC_ProgramTorqueRampMotor1(g_motor_command.goal_torque, 1);
    MC_StartMotor1();
}

void motor_stop()
{
  MC_StopMotor1();
  g_motor_started = false;
  g_motor_command.goal_torque = 0;
}

bool motor_in_position(int16_t position)
{
	if (abs(g_motor_status.current_hall_encoder_position - position) < 2) return true;

	return false;
}

bool motor_in_electric_rotation(int16_t electric_rotation)
{
	if (g_motor_status.current_electric_rotation - electric_rotation == 0) return true;

	return false;
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if(htim->Instance == TIM7) // 10 000 Hz
	{

		// 10 000 Hz
		{
			// READ TORQUE
			g_motor_status._current_torque_data[g_motor_status._current_torque_index++] = MC_GetPhaseCurrentAmplitudeMotor1();
			g_motor_status._current_torque_index %= CURRENT_TORQUE_DATA_SIZE;


			// READ MA730
			if (g_joint_configuration.ma730_exists)
			{
				g_motor_status.current_ma730_value = read_ma730_value();
			}

			// READ CURRENT SPEED
			g_motor_status._current_hall_speed_data[g_motor_status._current_hall_speed_index++] = g_motor_status.current_hall_encoder_position - g_motor_status.previous_hall_encoder_position;
			g_motor_status._current_hall_speed_index %= SPEED_DATA_SIZE;
			g_motor_status.previous_hall_encoder_position 	= g_motor_status.current_hall_encoder_position;

			g_counter_10000hz++;

		}

		// 1000 Hz
		if (g_timer_counter % 10 == 0)
		{
			g_counter_1000hz++;

			// Get states
			g_motor_status.current_voltage 			= (double) RealBusVoltageSensorParamsM1.aBuffer[RealBusVoltageSensorParamsM1.index] / 1036.6428571428;
			g_motor_status.current_torque 			= MC_GetPhaseCurrentAmplitudeMotor1();
			g_motor_status.stm_state_motor 			= MC_GetSTMStateMotor1();
			g_motor_status.mc_current_faults_motor 	= MC_GetCurrentFaultsMotor1();
			g_motor_status.mc_occured_faults_motor 	= MC_GetOccurredFaultsMotor1();
			g_motor_status.current_temperature 		= (uint8_t) NTC_GetAvTemp_C(&TempSensorParamsM1);
			if (g_motor_status.mc_current_faults_motor > 0 || g_motor_status.mc_occured_faults_motor > 0) {
				g_motor_status.errors =  g_motor_status.errors | JOINT_MC_FAILED;
			}

//			RecalculateMotorState(1000);

//			double l_time_period = 1.0 / 1000.0;

			// POSITION
			g_motor_status.current_joint_position_in_rad = M_TWOPI * ( (double) g_motor_status.current_hall_encoder_position / (double) ( g_joint_configuration.gear_ratio * g_joint_configuration.pole_pairs * 6) );

			// GOAL TORQUE
			g_motor_command.goal_motor_torque_in_nm	= (((double) g_motor_command.goal_torque) / 32768.0) * MAX_READABLE_CURRENT * 0.1118;
			g_motor_command.goal_joint_torque_in_nm	= g_motor_command.goal_motor_torque_in_nm * g_joint_configuration.gear_ratio;

			// CURRENT TORQUE
			double temp_torque = 0;
			for (int i = 0; i < CURRENT_TORQUE_DATA_SIZE; i++)
			{
				temp_torque += (double) g_motor_status._current_torque_data[i];
			}
			g_motor_status.current_torque 				= (int16_t) (temp_torque / CURRENT_TORQUE_DATA_SIZE);
			g_motor_status.current_motor_torque_in_nm	= (((double) g_motor_status.current_torque) / 32768.0) * MAX_READABLE_CURRENT * 0.1118;
			g_motor_status.current_joint_torque_in_nm	= g_motor_status.current_motor_torque_in_nm * g_joint_configuration.gear_ratio;

			// CURRENT SPEED
			int32_t temp_speed = 0;
			for (int i = 0; i < SPEED_DATA_SIZE; i++)
			{
				temp_speed += g_motor_status._current_hall_speed_data[i];
			}

			g_motor_status.current_hall_encoder_speed		= (double) temp_speed * 100.0; // to convert to sec
			double l_joint_diff_position_in_rad 			= M_TWOPI * ((double) (g_motor_status.current_hall_encoder_speed) / (double) ( g_joint_configuration.gear_ratio * g_joint_configuration.pole_pairs * 6) );
			g_motor_status.current_joint_speed_in_rads 		= (l_joint_diff_position_in_rad ); // in 100ms
			g_motor_status.current_joint_speed_in_rads_int  = (g_motor_status.current_joint_speed_in_rads * (32768.0 / M_TWOPI));

//			g_motor_status.current_motor_speed_in_rpm  		= ((g_motor_status.current_hall_encoder_speed * l_time_period) / 84) * 60;

			FSM_Tick();

			if ((g_motor_status.mc_current_faults_motor > 0 || g_motor_status.mc_occured_faults_motor > 0 ) && FSM_GetState() != FSM_FAULT)
			{
				FSM_Transition14(); // RAISE ERROR
			}

		}

		// 100 Hz
		if (g_timer_counter % 100 == 0)
		{

			g_counter_100hz++;

		}

		// 10 Hz
		if (g_timer_counter % 1000 == 0)
		{
			g_counter_10hz++;

		}

		// 1 Hz
		if (g_timer_counter % 10000 == 0)
		{
//			// SEND HEARTBEAT BY CANBUS
//			if (g_can_heartbeat == CAN_HEARTBEAT_ENABLED)
//			{
//				const uint8_t _can_start_address = 10;
//
//				FDCAN_TxHeaderTypeDef can_tx_header; // CAN Bus Receive Header
//				uint8_t can_tx_data[8]  = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
//
//				can_tx_header.Identifier = (g_joint_id + _can_start_address);
//				can_tx_header.IdType = FDCAN_STANDARD_ID;
//				can_tx_header.TxFrameType = FDCAN_DATA_FRAME;
//				can_tx_header.DataLength = FDCAN_DLC_BYTES_6;
//				can_tx_header.ErrorStateIndicator = FDCAN_ESI_ACTIVE;
//				can_tx_header.BitRateSwitch = FDCAN_BRS_ON;
//				can_tx_header.FDFormat = FDCAN_FD_CAN;
//				can_tx_header.TxEventFifoControl = FDCAN_NO_TX_EVENTS;
//				can_tx_header.MessageMarker = 0;
//
//				if (HAL_FDCAN_AddMessageToTxFifoQ(&hfdcan1, &can_tx_header, can_tx_data) == HAL_OK)
//				{
//					g_can_tx_counter++;
//				}
//
//			}
			g_counter_1hz++;
		}

		g_timer_counter++;

		// reset counter
		if (g_timer_counter == 10000) {
			g_timer_counter = 0;
		}
	}

	if(htim->Instance == TIM6) // 100Hz
	{
//		if (g_security_enabled == SECURITY_ENABLED)
//		{
//			FSM_Transition4(); // RAISE ERROR
//		}
	}
}

void Flash_Read_Data(uint32_t StartPageAddress, uint32_t *RxBuf, uint16_t numberofwords)
{
	while (1)
	{
		*RxBuf = *(__IO uint32_t *)StartPageAddress;
		StartPageAddress += 4;
		RxBuf++;
		if (!(numberofwords--)) break;
	}
}

int16_t get_sector_number_from_electric_rotation_number(int16_t electric_rotation)
{
	int16_t l_sector_number 		= -1;

	l_sector_number = (electric_rotation - g_joint_configuration.calibration_sector_size + 1) / g_joint_configuration.calibration_sector_size;

	return l_sector_number;
}

int16_t get_sector_number_from_ma730(int16_t ma730_value)
{
	int16_t l_sector_number 		= -1;

	// in sector
	for (int i = 0; i < g_joint_configuration.calibration_table_size; i++)
	{
		volatile uint16_t l_lower_value 		= 0;
		volatile uint16_t l_higher_value 	= 0;
		volatile bool  	  l_sector_with_zero	= false;
		volatile bool  	  l_wynik;

		l_higher_value 	= g_joint_configuration.calibration_table_1[i];
		l_lower_value 	= g_joint_configuration.calibration_table_2[i];

		if (l_higher_value < l_lower_value)
		{
			volatile uint16_t l_tmp_value = l_higher_value;
			l_higher_value 		 = l_lower_value;
			l_lower_value 		 = l_tmp_value;
		}

		l_wynik = (l_higher_value - l_lower_value > (uint16_t) 8000);

		if (l_wynik)
		{
			l_sector_with_zero = true;
		}

		if ((l_lower_value <= ma730_value && ma730_value <= l_higher_value) && !l_sector_with_zero)
		{
			return i;
		}

		if (( (0 <= ma730_value && ma730_value <= l_lower_value) || (l_higher_value <= ma730_value && ma730_value <= 16536) ) && l_sector_with_zero)
		{
			return i;
		}

//		if ((0 <= ma730_value && ma730_value <= l_higher_value) || (l_lower_value <= ma730_value && ma730_value <= 16536))
//		{
//			return i;
//		}

		//		if (l_lower_value > l_higher_value) // cross
//		{
//			if ((0 <= ma730_value && ma730_value <= l_higher_value) || (l_lower_value <= ma730_value && ma730_value <= 16536))
//			{
//				return i;
//			}
//		}

//		if (l_lower_value <= ma730_value && ma730_value <= l_higher_value)
//		{
//			return i;
//		}
	}

	// in space between readings
	if (g_current_sector_number != -1) // FIXME WORKAROUND: place between sectors - return last
	{
		return g_current_sector_number;
	}

	if (g_current_sector_number == -1) // FIXME WORKAROUND: place between sectors - no last sector, select farest from 0 position
	{
		uint16_t l_zero_sector = get_sector_number_from_electric_rotation_number(g_joint_configuration.zero_electric_rotation);
		for (int i = 1; i < g_joint_configuration.calibration_table_size; i++)
		{
			uint16_t l_lower_value 	= 0;
			uint16_t l_higher_value = 0;

			l_higher_value 	= g_joint_configuration.calibration_table_1[i];
			l_lower_value 	= g_joint_configuration.calibration_table_2[i + 1];

			if (l_lower_value <= ma730_value && ma730_value <= l_higher_value)
			{
//				return i;
				// select farest from 0
				if (i > l_zero_sector)
				{
					return g_joint_configuration.calibration_table_size;
				}
				else
				{
					return 0;
				}
			}
		}
	}

	FSM_Transition14(); // RAISE ERROR

	return l_sector_number;
}

int16_t calculate_encoder_diff_between_electric_rotation_numbers(int16_t electric_rotation_1, int16_t electric_rotation_2)
{
	return (electric_rotation_1 - electric_rotation_2) * 6;
}

int16_t calculate_diff_between_electric_rotation_numbers(int16_t electric_rotation_1, int16_t electric_rotation_2)
{
	return (electric_rotation_1 - electric_rotation_2);
}

int16_t calculate_encoder_diff_between_hall_states(uint8_t hall_state_1, uint8_t hall_state_2)
{
	uint8_t hall_table[6]  = {5, 4, 6, 2, 3, 1};
	int8_t i1 = -1;
	int8_t i2 = -1;
	for (uint8_t i = 0; i < 6; i++)
	{
		if (hall_table[i] == hall_state_1) i1 = i;
		if (hall_table[i] == hall_state_2) i2 = i;
	}

	return (i1 - i2);
}

int16_t calculate_encoder_between_electric_rotations_and_hall_states(int16_t electric_rotation_1, int16_t electric_rotation_2, uint8_t hall_state_1, uint8_t hall_state_2)
{

	int16_t encoder_diff_full_turns 	= calculate_encoder_diff_between_electric_rotation_numbers(electric_rotation_1, electric_rotation_2);

	int16_t encoder_diff_hall_states 	= calculate_encoder_diff_between_hall_states(hall_state_1, hall_state_2);

	return (encoder_diff_full_turns + encoder_diff_hall_states);
}

/* FSM States Callbacks ------------------------------------------------------*/
void FSM_StateStartCallback(void)
{
	// Automatic transition
//	FSM_Transition10();
	uint32_t flash_address_configuration 		= 0x0800F000;

	uint32_t flash_address_calibration_table 	= 0x0800F100;

	uint16_t g_calibration_config[16];
	uint32_t data[2];

	// KALIBRACJA ZAPISANA WE FLASH WYMAGANA
	Flash_Read_Data (flash_address_configuration, (uint32_t *) g_calibration_config, 8);

	// Read joint configuration from FLASH
	g_joint_configuration.pole_pairs = g_calibration_config[0];
	g_joint_configuration.gear_ratio = g_calibration_config[1];

	g_joint_configuration.calibration_sector_size 		= g_calibration_config[4];
	g_joint_configuration.maximum_electrical_rotations 	= g_calibration_config[5];

	g_joint_configuration.number_of_sectors 			= g_calibration_config[8];
	g_joint_configuration.calibration_table_size 		= g_calibration_config[9];

	g_joint_configuration.zero_hall_state 				= (uint8_t) g_calibration_config[12];
	g_joint_configuration.zero_electric_rotation 		= g_calibration_config[13];


	// Read calibration table from FLASH
//	for (int i = 0; i <= g_joint_configuration.calibration_table_size / 2 + 1; i++) {
	for (int i = 0; i <= g_joint_configuration.calibration_table_size; i++) {
		Flash_Read_Data (flash_address_calibration_table + i * 8, (uint32_t *) data, 1);
		g_joint_configuration.calibration_table_1[i]	= data[0] >> 16;
		g_joint_configuration.calibration_table_2[i] 	= data[0];
	}

	if (g_joint_configuration.calibration_table_size > 0) {
		g_joint_configuration.calibration_state = JOINT_CALIBRATED;
	}
	g_joint_configuration.hall_working_area = (6 * g_joint_configuration.pole_pairs * g_joint_configuration.gear_ratio) * 165.0 / 360.0;
	// --- DLA TOMKA (BRAK KALIBRACJI WE FLASH)
//	g_joint_configuration.pole_pairs = 14;
//	g_joint_configuration.gear_ratio = 120;
//	g_joint_configuration.calibration_state = JOINT_CALIBRATED;
	// --- END DLA TOMKA ---------------------

	g_node_status.security = SECURITY_DISABLED;
	g_node_status.hartbeat = HEARTBEAT_DISABLED;
	g_node_status.node_type = JOINT_BIG;

	// Configure CAN
	const uint8_t _can_start_address = 10;

	uint8_t _dip1 = (HAL_GPIO_ReadPin(GPIOB, DIP1_Pin) == GPIO_PIN_RESET) ? (8) : (0);
	uint8_t _dip2 = (HAL_GPIO_ReadPin(GPIOB, DIP2_Pin) == GPIO_PIN_RESET) ? (4) : (0);
	uint8_t _dip3 = (HAL_GPIO_ReadPin(GPIOB, DIP3_Pin) == GPIO_PIN_RESET) ? (2) : (0);
	uint8_t _dip4 = (HAL_GPIO_ReadPin(GPIOB, DIP4_Pin) == GPIO_PIN_RESET) ? (1) : (0);
	g_node_status.can_node_id = _dip2 + _dip3 + _dip4;

	// disable dip4 allow multiturn
	g_joint_configuration.working_area_constrain = _dip1;
	g_joint_configuration.ma730_exists = _dip1;

	// CAN FILTER
	g_can_filter_config.IdType = FDCAN_STANDARD_ID;
	g_can_filter_config.FilterIndex = 0;
	g_can_filter_config.FilterType = FDCAN_FILTER_DUAL;
	g_can_filter_config.FilterConfig = FDCAN_FILTER_TO_RXFIFO0;
	g_can_filter_config.FilterID1 = g_node_status.can_node_id + _can_start_address;
	g_can_filter_config.FilterID2 = 0x0AA ;

	// CAN TRANSMITION CONFIGURATION
	g_can_tx_header.Identifier = (g_node_status.can_node_id + _can_start_address) * 16;
	g_can_tx_header.IdType = FDCAN_STANDARD_ID;
	g_can_tx_header.TxFrameType = FDCAN_DATA_FRAME;
	g_can_tx_header.DataLength = FDCAN_DLC_BYTES_12;
	g_can_tx_header.ErrorStateIndicator = FDCAN_ESI_ACTIVE;
	g_can_tx_header.BitRateSwitch = FDCAN_BRS_ON;
	g_can_tx_header.FDFormat = FDCAN_FD_CAN;
	g_can_tx_header.TxEventFifoControl = FDCAN_NO_TX_EVENTS;
	g_can_tx_header.MessageMarker = 0;

	HAL_FDCAN_ConfigTxDelayCompensation(&hfdcan1, 10, 0);
	HAL_FDCAN_EnableTxDelayCompensation(&hfdcan1);

	HAL_FDCAN_ConfigFilter(&hfdcan1, &g_can_filter_config); //Initialize CAN Filter
	HAL_FDCAN_ConfigGlobalFilter(&hfdcan1, 3, 3, FDCAN_FILTER_REMOTE, FDCAN_REJECT_REMOTE);
	HAL_FDCAN_Start(&hfdcan1); //Initialize CAN Bus
	HAL_FDCAN_ActivateNotification(&hfdcan1, FDCAN_IT_RX_FIFO0_NEW_MESSAGE, 0);// Initialize CAN Bus Rx Interrupt
	HAL_FDCAN_EnableISOMode(&hfdcan1);

	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_SET); // Enable Gate driver

	// START TIMERS
    HAL_TIM_Base_Start_IT(&htim7); // Enable 10 kHz timer
    HAL_TIM_Base_Start_IT(&htim6); // Enable timer for watchdog

    FSM_Transition10(); // FIXME
}

void FSM_StateInitCallback(void)
{
	if (g_joint_configuration.ma730_exists)
	{
		g_current_sector_number  = get_sector_number_from_ma730(g_motor_status.current_ma730_value);

		if (g_current_sector_number > -1 && g_previous_sector_number > -1) // przeszedl 1 krok iteracji
		{
			switch (g_motor_status.encoder_position_state)
			{
				case POSITION_UNKNOWN:
				{
					if (g_current_sector_number != -1)
					{
						g_motor_status.encoder_position_state = POSITION_APROXIMATED;
					}
					else
					{
						g_motor_status.encoder_position_state = POSITION_ESTIMATION_FAILED;
						g_motor_status.errors =  g_motor_status.errors | JOINT_POSITION_ENCODER_FAILED;
						FSM_Transition14(); // RAISE ERROR
						return;
					}
					break;
				}

				case POSITION_APROXIMATED: // Searching exact sector
				{
					g_zero_sector_number = get_sector_number_from_electric_rotation_number(g_joint_configuration.zero_electric_rotation);

					if (g_current_sector_number - g_zero_sector_number < 0) // jest z prawej, idz CW
					{
						if (!g_motor_started ) motor_start_cw(speed_limit);

						// SPEED REGULATOR
						if (g_motor_status.current_hall_encoder_speed > -1 * speed_limit)
						{
							motor_speed_up_cw(speed_limit, increase_torque_step);
						}
						else
						{
							motor_slow_down_cw(speed_limit, increase_torque_step);
						}
					}

					if (g_current_sector_number - g_zero_sector_number > 0) // jest z lewej, idz CCW
					{
						if (!g_motor_started) motor_start_ccw(speed_limit);

						// SPEED REGULATOR
						if (g_motor_status.current_hall_encoder_speed < speed_limit)
						{
							motor_speed_up_ccw(speed_limit, increase_torque_step);
						}
						else
						{
							motor_slow_down_ccw(speed_limit, increase_torque_step);
						}
					}

					// current sector number is changed - aproximate current_electric_rotation
					if (g_current_sector_number - g_previous_sector_number > 0)
					{
						g_motor_status.current_electric_rotation 		= (g_current_sector_number - 1) * g_joint_configuration.calibration_sector_size + 1;
						g_motor_status.current_hall_encoder_position 	= calculate_encoder_between_electric_rotations_and_hall_states(g_motor_status.current_electric_rotation, g_joint_configuration.zero_electric_rotation, g_motor_status.current_hall_state, g_joint_configuration.zero_hall_state);
						g_motor_status.encoder_position_state 			= POSITION_ACCURATE;
					}

					if (g_previous_sector_number - g_current_sector_number > 0)
					{
						g_motor_status.current_electric_rotation 		= g_current_sector_number * g_joint_configuration.calibration_sector_size;
						g_motor_status.current_hall_encoder_position 	= calculate_encoder_between_electric_rotations_and_hall_states(g_motor_status.current_electric_rotation, g_joint_configuration.zero_electric_rotation, g_motor_status.current_hall_state, g_joint_configuration.zero_hall_state);
						g_motor_status.encoder_position_state 			= POSITION_ACCURATE;
					}

					break;
				}

				case POSITION_ACCURATE:
				{
					motor_stop();
					FSM_Transition11();
					break;
				}

				case POSITION_ESTIMATION_FAILED:
				{
					FSM_Transition14(); // RAISE ERROR
					return;
					break;
				}

			}
		}

		g_previous_sector_number = g_current_sector_number;
	}
	else // Multiturn not using MA730
	{
		g_motor_status.encoder_position_state = POSITION_ACCURATE;
		FSM_Transition11();
	}

}

void FSM_StateReadyToOperateCallback(void)
{
	FSM_Transition12(); // FIXME - GO TO OPERATION_ENABLE
}

void FSM_StateOperationEnableCallback(void)
{
	switch (g_motor_status.current_joint_position)
	{
	case POSITION_UNDER_WORKING_AREA: // Accept only positive torque
		if (g_motor_command.goal_torque < 0)
		{
			motor_stop();
		}
		else
		{
			MC_ProgramTorqueRampMotor1(g_motor_command.goal_torque, 0);
			MC_StartMotor1();
		}
		break;

	case POSITION_IN_WORKING_AREA:
		MC_ProgramTorqueRampMotor1(g_motor_command.goal_torque, 0);
		MC_StartMotor1();
		break;

	case POSITION_OVER_WORKING_AREA: // Accept only negative torque
		if (g_motor_command.goal_torque > 0)
		{
			motor_stop();
		}
		else
		{
			MC_ProgramTorqueRampMotor1(g_motor_command.goal_torque, 0);
			MC_StartMotor1();
		}
		break;
	}
}

void FSM_StateFaultReactionActiveCallback(void)
{
	MC_StopMotor1();
}

//void FSM_StateFaultCallback(void)
//{
//
//}

/* FSM Transition Callbacks --------------------------------------------------*/
void FSM_Transition10Callback(void)
{
}

void FSM_Transition11Callback(void)
{
}

void FSM_Transition12Callback(void)
{
	MC_StartMotor1();
}

void FSM_Transition13Callback(void)
{
	MC_StopMotor1();
}

void FSM_Transition14Callback(void)
{
	MC_StopMotor1();
}

void FSM_Transition15Callback(void)
{
	MC_AcknowledgeFaultMotor1();
	g_motor_status.mc_current_faults_motor = 0;
	g_motor_status.mc_occured_faults_motor = 0;
	g_motor_status.errors = 0;
}

// Current FDBK
void R3_2_GetPhaseCurrents( PWMC_Handle_t * pHdl, ab_t * Iab )
{
#if defined (__ICCARM__)
  #pragma cstat_disable = "MISRAC2012-Rule-11.3"
#endif /* __ICCARM__ */
  PWMC_R3_2_Handle_t * pHandle = ( PWMC_R3_2_Handle_t * )pHdl;
#if defined (__ICCARM__)
  #pragma cstat_restore = "MISRAC2012-Rule-11.3"
#endif /* __ICCARM__ */
  TIM_TypeDef * TIMx = pHandle->pParams_str->TIMx;

  uint8_t Sector;
  int32_t Aux;
  uint32_t ADCDataReg1;
  uint32_t ADCDataReg2;

  Sector = ( uint8_t )pHandle->_Super.Sector;
  ADCDataReg1 = *pHandle->pParams_str->ADCDataReg1[Sector];
  ADCDataReg2 = *pHandle->pParams_str->ADCDataReg2[Sector];

  /* disable ADC trigger source */
  //LL_TIM_CC_DisableChannel(TIMx, LL_TIM_CHANNEL_CH4);
  LL_TIM_SetTriggerOutput(TIMx, LL_TIM_TRGO_RESET);

  switch ( Sector )
  {
    case SECTOR_4:
    case SECTOR_5:
      /* Current on Phase C is not accessible     */
      /* Ia = PhaseAOffset - ADC converted value) */
//      Aux = ( int32_t )( pHandle->PhaseAOffset ) - ( int32_t )( ADCDataReg1 );
		Aux = (int32_t) (ADCDataReg1) - (int32_t) (pHandle->PhaseAOffset);

      /* Saturation of Ia */
      if ( Aux < -INT16_MAX )
      {
        Iab->a = -INT16_MAX;
      }
      else  if ( Aux > INT16_MAX )
      {
        Iab->a = INT16_MAX;
      }
      else
      {
        Iab->a = ( int16_t )Aux;
      }

      /* Ib = PhaseBOffset - ADC converted value) */
//      Aux = ( int32_t )( pHandle->PhaseBOffset ) - ( int32_t )( ADCDataReg2 );
      Aux = ( int32_t )( ADCDataReg2 ) - ( int32_t )( pHandle->PhaseBOffset );

      /* Saturation of Ib */
      if ( Aux < -INT16_MAX )
      {
        Iab->b = -INT16_MAX;
      }
      else  if ( Aux > INT16_MAX )
      {
        Iab->b = INT16_MAX;
      }
      else
      {
        Iab->b = ( int16_t )Aux;
      }
      break;

    case SECTOR_6:
    case SECTOR_1:
      /* Current on Phase A is not accessible     */
      /* Ib = PhaseBOffset - ADC converted value) */
//      Aux = ( int32_t )( pHandle->PhaseBOffset ) - ( int32_t )( ADCDataReg1 );
    	Aux = ( int32_t )( ADCDataReg1 ) - ( int32_t )( pHandle->PhaseBOffset );

      /* Saturation of Ib */
      if ( Aux < -INT16_MAX )
      {
        Iab->b = -INT16_MAX;
      }
      else  if ( Aux > INT16_MAX )
      {
        Iab->b = INT16_MAX;
      }
      else
      {
        Iab->b = ( int16_t )Aux;
      }

      /* Ia = -Ic -Ib */
//      Aux = ( int32_t )( ADCDataReg2 ) - ( int32_t )( pHandle->PhaseCOffset ); /* -Ic */
      Aux = ( int32_t )( pHandle->PhaseCOffset ) - ( int32_t )( ADCDataReg2 ); /* Ic reversed */
      Aux -= ( int32_t )Iab->b;             /* Ia  */

      /* Saturation of Ia */
      if ( Aux > INT16_MAX )
      {
        Iab->a = INT16_MAX;
      }
      else  if ( Aux < -INT16_MAX )
      {
        Iab->a = -INT16_MAX;
      }
      else
      {
        Iab->a = ( int16_t )Aux;
      }
      break;

    case SECTOR_2:
    case SECTOR_3:
      /* Current on Phase B is not accessible     */
      /* Ia = PhaseAOffset - ADC converted value) */
//      Aux = ( int32_t )( pHandle->PhaseAOffset ) - ( int32_t )( ADCDataReg1 );
    	Aux = ( int32_t )( ADCDataReg1 ) - ( int32_t )( pHandle->PhaseAOffset );

      /* Saturation of Ia */
      if ( Aux < -INT16_MAX )
      {
        Iab->a = -INT16_MAX;
      }
      else  if ( Aux > INT16_MAX )
      {
        Iab->a = INT16_MAX;
      }
      else
      {
        Iab->a = ( int16_t )Aux;
      }

      /* Ib = -Ic -Ia */
//      Aux = ( int32_t )( ADCDataReg2 ) - ( int32_t )( pHandle->PhaseCOffset ); /* -Ic */
      Aux = ( int32_t )( pHandle->PhaseCOffset ) - ( int32_t )( ADCDataReg2 ); /* -Ic */
      Aux -= ( int32_t )Iab->a;             /* Ib */

      /* Saturation of Ib */
      if ( Aux > INT16_MAX )
      {
        Iab->b = INT16_MAX;
      }
      else  if ( Aux < -INT16_MAX )
      {
        Iab->b = -INT16_MAX;
      }
      else
      {
        Iab->b = ( int16_t )Aux;
      }
      break;

    default:
      break;
  }

  pHandle->_Super.Ia = Iab->a;
  pHandle->_Super.Ib = Iab->b;
  pHandle->_Super.Ic = -Iab->a - Iab->b;
}

// HALL FDBK
void * HALL_TIMx_CC_IRQHandler( void * pHandleVoid )
{
  HALL_Handle_t * pHandle = ( HALL_Handle_t * ) pHandleVoid;
  TIM_TypeDef * TIMx = pHandle->TIMx;
  uint8_t bPrevHallState;
  int8_t PrevDirection;
  uint32_t wCaptBuf;
  uint16_t hPrscBuf;
  uint16_t hHighSpeedCapture;

  if ( pHandle->SensorIsReliable )
  {
    /* A capture event generated this interrupt */
    bPrevHallState = pHandle->HallState;
    PrevDirection = pHandle->Direction;

    if ( pHandle->SensorPlacement == DEGREES_120 )
    {
      pHandle->HallState  =(uint8_t) ((LL_GPIO_IsInputPinSet( pHandle->H3Port, pHandle->H3Pin ) << 2)
                            | (LL_GPIO_IsInputPinSet( pHandle->H2Port, pHandle->H2Pin ) << 1)
                            | LL_GPIO_IsInputPinSet( pHandle->H1Port, pHandle->H1Pin ) );
    }
    else
    {
      pHandle->HallState  = (uint8_t) ((( LL_GPIO_IsInputPinSet( pHandle->H2Port, pHandle->H2Pin ) ^ 1 ) << 2)
                            | (LL_GPIO_IsInputPinSet( pHandle->H3Port, pHandle->H3Pin ) << 1)
                            | LL_GPIO_IsInputPinSet( pHandle->H1Port, pHandle->H1Pin ) );
    }

    g_motor_status.current_hall_state = pHandle->HallState;
    switch ( pHandle->HallState )
    {
      case STATE_5:
        if ( bPrevHallState == STATE_4 )
        {
          pHandle->Direction = POSITIVE;
          pHandle->MeasuredElAngle = pHandle->PhaseShift;
          g_motor_status.current_hall_encoder_position++;
        }
        else if ( bPrevHallState == STATE_1 )
        {
          pHandle->Direction = NEGATIVE;
          pHandle->MeasuredElAngle = ( int16_t )( pHandle->PhaseShift + S16_60_PHASE_SHIFT );
          g_motor_status.current_hall_encoder_position--;
          g_motor_status.current_electric_rotation--;
        }
        else
        {
        }
        break;

      case STATE_1:
        if ( bPrevHallState == STATE_5 )
        {
          pHandle->Direction = POSITIVE;
          pHandle->MeasuredElAngle = pHandle->PhaseShift + S16_60_PHASE_SHIFT;
          g_motor_status.current_hall_encoder_position++;
          g_motor_status.current_electric_rotation++;
        }
        else if ( bPrevHallState == STATE_3 )
        {
          pHandle->Direction = NEGATIVE;
          pHandle->MeasuredElAngle = ( int16_t )( pHandle->PhaseShift + S16_120_PHASE_SHIFT );
          g_motor_status.current_hall_encoder_position--;
        }
        else
        {
        }
        break;

      case STATE_3:
        if ( bPrevHallState == STATE_1 )
        {
          pHandle->Direction = POSITIVE;
          pHandle->MeasuredElAngle = ( int16_t )( pHandle->PhaseShift + S16_120_PHASE_SHIFT );
          g_motor_status.current_hall_encoder_position++;
        }
        else if ( bPrevHallState == STATE_2 )
        {
          pHandle->Direction = NEGATIVE;
          pHandle->MeasuredElAngle = ( int16_t )( pHandle->PhaseShift + S16_120_PHASE_SHIFT +
                                                  S16_60_PHASE_SHIFT );
          g_motor_status.current_hall_encoder_position--;
        }
        else
        {
        }

        break;

      case STATE_2:
        if ( bPrevHallState == STATE_3 )
        {
          pHandle->Direction = POSITIVE;
          pHandle->MeasuredElAngle = ( int16_t )( pHandle->PhaseShift + S16_120_PHASE_SHIFT
                                                  + S16_60_PHASE_SHIFT );
          g_motor_status.current_hall_encoder_position++;
        }
        else if ( bPrevHallState == STATE_6 )
        {
          pHandle->Direction = NEGATIVE;
          pHandle->MeasuredElAngle = ( int16_t )( pHandle->PhaseShift - S16_120_PHASE_SHIFT );
          g_motor_status.current_hall_encoder_position--;
        }
        else
        {
        }
        break;

      case STATE_6:
        if ( bPrevHallState == STATE_2 )
        {
          pHandle->Direction = POSITIVE;
          pHandle->MeasuredElAngle = ( int16_t )( pHandle->PhaseShift - S16_120_PHASE_SHIFT );
          g_motor_status.current_hall_encoder_position++;
        }
        else if ( bPrevHallState == STATE_4 )
        {
          pHandle->Direction = NEGATIVE;
          pHandle->MeasuredElAngle = ( int16_t )( pHandle->PhaseShift - S16_60_PHASE_SHIFT );
          g_motor_status.current_hall_encoder_position--;
        }
        else
        {
        }
        break;

      case STATE_4:
        if ( bPrevHallState == STATE_6 )
        {
          pHandle->Direction = POSITIVE;
          pHandle->MeasuredElAngle = ( int16_t )( pHandle->PhaseShift - S16_60_PHASE_SHIFT );
          g_motor_status.current_hall_encoder_position++;
        }
        else if ( bPrevHallState == STATE_5 )
        {
          pHandle->Direction = NEGATIVE;
          pHandle->MeasuredElAngle = ( int16_t )( pHandle->PhaseShift );
          g_motor_status.current_hall_encoder_position--;
        }
        else
        {
        }
        break;

      default:
        /* Bad hall sensor configutarion so update the speed reliability */
        pHandle->SensorIsReliable = false;

        break;
    }
    /* We need to check that the direction has not changed.
       If it is the case, the sign of the current speed can be the opposite of the
       average speed, and the average time can be close to 0 which lead to a
       computed speed close to the infinite, and bring instability. */
    if (pHandle->Direction != PrevDirection)
    {
      /* Setting BufferFilled to 0 will prevent to compute the average speed based
       on the SpeedPeriod buffer values */
      pHandle->BufferFilled = 0 ;
      pHandle->SpeedFIFOIdx = 0;
    }

    if (pHandle->HallMtpa == true)
    {
      pHandle->_Super.hElAngle = pHandle->MeasuredElAngle;
    }
    else
    {
      /* Nothing to do */
    }

    /* Discard first capture */
    if ( pHandle->FirstCapt == 0u )
    {
      pHandle->FirstCapt++;
      LL_TIM_IC_GetCaptureCH1( TIMx );
    }
    else
    {
      /* used to validate the average speed measurement */
      if ( pHandle->BufferFilled < pHandle->SpeedBufferSize )
      {
        pHandle->BufferFilled++;
      }

      /* Store the latest speed acquisition */
      hHighSpeedCapture = LL_TIM_IC_GetCaptureCH1( TIMx );
      wCaptBuf = ( uint32_t )hHighSpeedCapture;
      hPrscBuf =  LL_TIM_GetPrescaler ( TIMx );

      /* Add the numbers of overflow to the counter */
      wCaptBuf += ( uint32_t )pHandle->OVFCounter * 0x10000uL;

      if ( pHandle->OVFCounter != 0u )
      {
        /* Adjust the capture using prescaler */
        uint16_t hAux;
        hAux = hPrscBuf + 1u;
        wCaptBuf *= hAux;

        if ( pHandle->RatioInc )
        {
          pHandle->RatioInc = false;  /* Previous capture caused overflow */
          /* Don't change prescaler (delay due to preload/update mechanism) */
        }
        else
        {
          if ( LL_TIM_GetPrescaler ( TIMx ) < pHandle->HALLMaxRatio ) /* Avoid OVF w/ very low freq */
          {
            LL_TIM_SetPrescaler ( TIMx, LL_TIM_GetPrescaler ( TIMx ) + 1 ); /* To avoid OVF during speed decrease */
            pHandle->RatioInc = true;   /* new prsc value updated at next capture only */
          }
        }
      }
      else
      {
        /* If prsc preload reduced in last capture, store current register + 1 */
        if ( pHandle->RatioDec ) /* and don't decrease it again */
        {
          /* Adjust the capture using prescaler */
          uint16_t hAux;
          hAux = hPrscBuf + 2u;
          wCaptBuf *= hAux;

          pHandle->RatioDec = false;
        }
        else  /* If prescaler was not modified on previous capture */
        {
          /* Adjust the capture using prescaler */
          uint16_t hAux = hPrscBuf + 1u;
          wCaptBuf *= hAux;

          if ( hHighSpeedCapture < LOW_RES_THRESHOLD ) /* If capture range correct */
          {
            if ( LL_TIM_GetPrescaler ( TIMx ) > 0u ) /* or prescaler cannot be further reduced */
            {
              LL_TIM_SetPrescaler ( TIMx, LL_TIM_GetPrescaler ( TIMx ) - 1 ); /* Increase accuracy by decreasing prsc */
              /* Avoid decrementing again in next capt.(register preload delay) */
              pHandle->RatioDec = true;
            }
          }
        }
      }

      /* Filtering to fast speed... could be a glitch  ? */
      /* the HALL_MAX_PSEUDO_SPEED is temporary in the buffer, and never included in average computation*/
        if ( wCaptBuf < pHandle->MinPeriod )
        {
           /* pHandle->AvrElSpeedDpp = HALL_MAX_PSEUDO_SPEED; */
        }
        else
        {
          pHandle->ElPeriodSum -= pHandle->SensorPeriod[pHandle->SpeedFIFOIdx]; /* value we gonna removed from the accumulator */
          if ( wCaptBuf >= pHandle->MaxPeriod )
          {
            pHandle->SensorPeriod[pHandle->SpeedFIFOIdx] = pHandle->MaxPeriod*pHandle->Direction;
          }
          else
          {
            pHandle->SensorPeriod[pHandle->SpeedFIFOIdx] = wCaptBuf ;
            pHandle->SensorPeriod[pHandle->SpeedFIFOIdx] *= pHandle->Direction;
            pHandle->ElPeriodSum += pHandle->SensorPeriod[pHandle->SpeedFIFOIdx];
          }
          /* Update pointers to speed buffer */
          pHandle->SpeedFIFOIdx++;
          if ( pHandle->SpeedFIFOIdx == pHandle->SpeedBufferSize )
          {
            pHandle->SpeedFIFOIdx = 0u;
          }
          if ( pHandle->SensorIsReliable)
          {
            if ( pHandle->BufferFilled < pHandle->SpeedBufferSize )
            {
              pHandle->AvrElSpeedDpp = ( int16_t ) (( pHandle->PseudoFreqConv / wCaptBuf )*pHandle->Direction);
            }
            else
            { /* Average speed allow to smooth the mechanical sensors misalignement */
              pHandle->AvrElSpeedDpp = ( int16_t )((int32_t) pHandle->PseudoFreqConv / ( pHandle->ElPeriodSum / pHandle->SpeedBufferSize )); /* Average value */

            }
          }
          else /* Sensor is not reliable */
          {
            pHandle->AvrElSpeedDpp = 0;
          }
        }
      /* Reset the number of overflow occurred */
      pHandle->OVFCounter = 0u;
    }
  }

	if (g_motor_status.current_hall_encoder_position < -1 * g_joint_configuration.hall_working_area && g_joint_configuration.working_area_constrain)
	{
		g_motor_status.current_joint_position = POSITION_UNDER_WORKING_AREA;
		g_motor_status.warnings = g_motor_status.warnings | 0x01;
	}
	else if(g_motor_status.current_hall_encoder_position > g_joint_configuration.hall_working_area && g_joint_configuration.working_area_constrain)
	{
		g_motor_status.current_joint_position = POSITION_OVER_WORKING_AREA;
		g_motor_status.warnings = g_motor_status.warnings | 0x01;
	}
	else
	{
		g_motor_status.current_joint_position = POSITION_IN_WORKING_AREA;
		g_motor_status.warnings = g_motor_status.warnings & 0xFE;
	}

  return MC_NULL;
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
