/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "motorcontrol.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/*
	   * HALL PHASE SHIFT
	   *
	   * PHASE:	1	2	3	4	5	6
	   *
	   * VALUE:	3	1	5	4	6	2
	   *
	   *
	   *Move software limit 	2.10 rad.
	   *Move hardware limit 	2.86 rad.
*/
#define PHASE_1 (uint8_t)3
#define PHASE_2 (uint8_t)1
#define PHASE_3 (uint8_t)5
#define PHASE_4 (uint8_t)4
#define PHASE_5 (uint8_t)6
#define PHASE_6 (uint8_t)2

// Safe margin value based on hall sensor:
// 1 mechanical motor rotation = 6 (hall sennsors) * 14 (pole pairs) = 84
#define SafeMargin (uint16_t) 336

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
ADC_HandleTypeDef hadc2;

CORDIC_HandleTypeDef hcordic;

SPI_HandleTypeDef hspi2;
DMA_HandleTypeDef hdma_spi2_rx;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim6;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */

// Calibration PHASE -------------------------------------------
uint8_t Cal_Phase = 0; //||||||||||||||||||||||||||||||||||||||
//---------------------------------------------------------------

// HALL ENCODER -------------------
int8_t HallStatus;
int8_t HallStatus2;
uint8_t bPrevHallStatus;
int16_t Position;
int16_t LastPosition = 12000;
// ------------------------------

// MOTOR CONTROL ----------------
int16_t Speed[4];
int16_t Torque;
// -----------------------------

// MA730 ENCODER ---------------
uint16_t SPI_Buffer2;
int16_t SPI_Buffer;
uint16_t MA730_Offset;
//-----------------------------

// LIMITS AND CALIBRATION -----
int16_t UpperEnd1;
int16_t LowerEnd1;
int16_t LowerEnd2;
int16_t Zero_position;
//-----------------------------

// FLSH BUFFERS --------------
uint64_t Program;
uint32_t ProgramBuffer[860];
uint8_t Sector;
uint16_t ProgramID;
uint32_t ProgramADDR;
uint16_t iteration;
uint16_t checekbuffer[4];

//---------------------------
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_ADC1_Init(void);
static void MX_ADC2_Init(void);
static void MX_CORDIC_Init(void);
static void MX_TIM1_Init(void);
static void MX_TIM2_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_SPI2_Init(void);
static void MX_TIM6_Init(void);
static void MX_NVIC_Init(void);
/* USER CODE BEGIN PFP */

void GetRotorPosition(void);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

	uint8_t calc = 0;
	uint8_t IsFlashWritten = 0 ;

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_ADC2_Init();
  MX_CORDIC_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_USART2_UART_Init();
  MX_MotorControl_Init();
  MX_SPI2_Init();
  MX_TIM6_Init();

  /* Initialize interrupts */
  MX_NVIC_Init();
  /* USER CODE BEGIN 2 */

  HAL_TIM_Base_Start_IT(&htim6);
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_SET);


  // Robot Initialization Hotfix
    HAL_Delay(1000);
    MC_ProgramTorqueRampMotor1(0, 100);
    MC_StartMotor1();
    HAL_Delay(200);
    MC_StopMotor1();
    HAL_Delay(500);
    Torque = 100;

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	  // Phase INITIALIZATON, START GO UP ---------------------------------------------------------------------------------------------------
	  while (Cal_Phase == 0)
	  {
		  if (MC_GetOccurredFaultsMotor1() == 0)
		  {
			  MC_ProgramTorqueRampMotor1(Torque, 10);
			  MC_StartMotor1();

			  for (int i = 0; i < 4; i++ )
			  {
				  Speed[i] = MC_GetMecSpeedAverageMotor1();
				  HAL_Delay(2);
			  }

			  if (Speed[0] <= -60 && Speed[1] <= -60 && Speed[2] <= -60 && Speed[3] <= -60 )
			  {
				  Cal_Phase = 1;
				  HAL_Delay(100);
			  }

			  if (Speed[0] >= -60 && Speed[1] >= -60 && Speed[2] >= -60 && Speed[3] >= -60 )
			  {
				  Torque += 25;
				  MC_ProgramTorqueRampMotor1(Torque, 10);
				  HAL_Delay(50);
			  }

		  }

	  }
	  // GOING UP TILL LIMIT HIT -----------------------------------------------------------------------------------------------------------
	  while (Cal_Phase == 1)
	  {
		  for (int i = 0; i < 4; i++ )
		  {
			  Speed[i] = MC_GetMecSpeedAverageMotor1();
			  HAL_Delay(2);
		  }

	/*	  if (Speed[0] >= -300 && Speed[1] >= -300 && Speed[2] >= -300 && Speed[3] >= -300 )
		  {
			  Torque = Torque - 25;
			  MC_ProgramTorqueRampMotor1(Torque, 1);
		  }
	*/
		  if (Speed[0] >= -40 && Speed[1] >= -40 && Speed[2] >= -40 && Speed[3] >= -40 && MC_GetOccurredFaultsMotor1() == 0)
		  {
			  MC_StopMotor1();
			  Torque = 0;
			//  UpperEnd1 = Position;
			  Cal_Phase = 2;
		  }
	  }
	  // HIT UP LIMIT ----------------------------------------------------------------------------------------------------------------------
	  while (Cal_Phase == 2)
	  {
		  for (int i = 0; i < 4; i++ )
		  {
			  Speed[i] = MC_GetMecSpeedAverageMotor1();
			  HAL_Delay(2);
		  }

	/*	  if (Speed[0] >= -300 && Speed[1] >= -300 && Speed[2] >= -300 && Speed[3] >= -300 )
		  {
			  Torque = Torque - 25;
			  MC_ProgramTorqueRampMotor1(Torque, 1);
		  }
	*/
		  if (Speed[0] >= -40 && Speed[1] >= -40 && Speed[2] >= -40 && Speed[3] >= -40 && MC_GetOccurredFaultsMotor1() == 0)
		  {
			  MC_StopMotor1();
			  Torque = 0;
			  UpperEnd1 = Position;
			  Cal_Phase = 3;
		  }

	  }

	  // HIT THE LIMIT, SET MOTOR TO GO DOWN ------------------------------------------------------------------------------------------------
	  while (Cal_Phase == 3)
	  {
		  MC_ProgramTorqueRampMotor1(Torque, 10);
		  MC_StartMotor1();

		  for (int i = 0; i < 4; i++ )
		  {
			  Speed[i] = MC_GetMecSpeedAverageMotor1();
			  HAL_Delay(2);
		  }

		  if (Speed[0] >= 60 && Speed[1] >= 60 && Speed[2] >= 60 && Speed[3] >= 60 )
			  {
				  Cal_Phase = 4;
			  }

		  if (Speed[0] <= 60 && Speed[1] <= 60 && Speed[2] <= 60 && Speed[3] <= 60 )
		  {
			  Torque = Torque - 25;
			  MC_ProgramTorqueRampMotor1(Torque, 10);
			  HAL_Delay(50);
		  }

	  }

	  // GOING DOWN TILL LIMIT HIT--------------------------------------------------------------------------------------------------------------------------
	  while (Cal_Phase == 4)
	  {
		  for (int i = 0; i < 4; i++ )
		  {
			  Speed[i] = MC_GetMecSpeedAverageMotor1();
			  HAL_Delay(2);
		  }

	/*	  if (Speed[0] <= 200 && Speed[1] <= 200 && Speed[2] <= 200 && Speed[3] <= 200)
		  {
			  Torque = Torque + 25;
			  MC_ProgramTorqueRampMotor1(Torque, 1);
		  }

	*/	  if (Speed[0] <= 40 && Speed[1] <= 40 && Speed[2] <= 40 && Speed[3] <= 40 && MC_GetOccurredFaultsMotor1() == 0)
		  {
			  MC_StopMotor1();
			  Torque = 0;
			  LowerEnd1 = Position;
			  LowerEnd2 = Position;
			  Cal_Phase = 5;
		  }
	  }

	  // LIMIT HIT, CALCULATION OF ZERO POSITION -------------------------------------------------------------------------------------------------------
	  while (Cal_Phase == 5)
	  {
		  if (calc == 0)
		  {
			  Zero_position = (LowerEnd2 + UpperEnd1) /2;
			  LowerEnd2 = LowerEnd2 - Zero_position;
			  UpperEnd1 = UpperEnd1 - Zero_position;
			  Position = Position - Zero_position;
			  calc = 1;
		  }


		  for (int i = 0; i < 4; i++ )
		  {
			  Speed[i] = MC_GetMecSpeedAverageMotor1();
			  HAL_Delay(2);
		  }

		  if (Speed[0] <= -60 && Speed[1] <= -60 && Speed[2] <= -60 && Speed[3] <= -60 )
		  {
			  Cal_Phase = 6;
			  HAL_Delay(100);
		  }

		  if (Speed[0] >= -60 && Speed[1] >= -60 && Speed[2] >= -60 && Speed[3] >= -60 )
		  {
			  Torque += 25;
			  MC_ProgramTorqueRampMotor1(Torque, 10);
			  MC_StartMotor1();
			  HAL_Delay(50);
		  }
	  }

	  // GO BACK TO ZERO POSITION ---------------------------------------------------------------------------------------------------------------------
	  while(Cal_Phase == 6)
	  {
		  if (Position >= 0)
		  {
			  MC_StopMotor1();
			  Cal_Phase = 7;
			  Torque = 0;
			  HAL_Delay(100);
		  }
		  HAL_Delay(0);
	  }


	  // IN ZERO POSITION, GET MA730 OFFSET, GO TO LOW END ---------------------------------------------------------------------------------------------
	  while(Cal_Phase == 7)
	  {
		  if (MA730_Offset == 0)
		  {
			  MA730_Offset = 0;
			  HAL_GPIO_WritePin(MA730_CS_GPIO_Port, MA730_CS_Pin, GPIO_PIN_RESET);
			  HAL_SPI_Receive(&hspi2, &MA730_Offset, 14, 100);
			  HAL_GPIO_WritePin(MA730_CS_GPIO_Port, MA730_CS_Pin, GPIO_PIN_SET);
			  MA730_Offset = MA730_Offset & 0b0011111111111111;
			  Torque = 0;

		  }
		  if (MA730_Offset != 0)
		  {
			  HAL_Delay(100);
			  MC_StartMotor1();

			  for (int i = 0; i < 4; i++ )
			  {
				  Speed[i] = MC_GetMecSpeedAverageMotor1();
				  HAL_Delay(2);
			  }
			  if (Speed[0] >= 60 && Speed[1] >= 60 && Speed[2] >= 60 && Speed[3] >= 60 )
			  {
				  Cal_Phase = 8;
				//  LowerEnd2 = LowerEnd2 + 168;
				//  UpperEnd1 = UpperEnd1 - 168;
			  }

			  if (Speed[0] <= 60 && Speed[1] <= 60 && Speed[2] <= 60 && Speed[3] <= 60 )
			  {
				  Torque = Torque - 25;
				  MC_ProgramTorqueRampMotor1(Torque, 10);
			  	  HAL_Delay(50);
		  	  }
		  }
	  }

	  // GOING TO LOW END -----------------------------------------------------------------------------------------------------------------------------
	  while (Cal_Phase == 8)
	  {
		  if (Position <= (LowerEnd2+2))
		  {
			  MC_StopMotor1();
			  Torque = 0;
			  Cal_Phase = 10;
			  ProgramBuffer[0] = UpperEnd1 << 16;
			  ProgramBuffer[0] = ProgramBuffer[0] + (UpperEnd1 - SafeMargin);
			  ProgramID = 1;
		  }
		  HAL_Delay(0);
	  }

	  //  IN LOW END, START DATA AQUISITION ---------------------------------------------------------------------------------------------------------
	  while (Cal_Phase == 10)
	  {
		  if (MC_GetOccurredFaultsMotor1() == 0)
		  {
			  MC_ProgramTorqueRampMotor1(Torque, 10);
			  MC_StartMotor1();

			  for (int i = 0; i < 4; i++ )
			  {
				  Speed[i] = MC_GetMecSpeedAverageMotor1();
				  HAL_Delay(2);
			  }

			  if (Speed[0] <= -60 && Speed[1] <= -60 && Speed[2] <= -60 && Speed[3] <= -60 )
			  {
				  Cal_Phase = 11;
				  HAL_Delay(100);
			  }

			  if (Speed[0] >= -60 && Speed[1] >= -60 && Speed[2] >= -60 && Speed[3] >= -60 )
			  {
				  Torque += 25;
				  MC_ProgramTorqueRampMotor1(Torque, 10);
				  HAL_Delay(50);
			  }

		  }
	  }
	  // HIT UP LIMIT ----------------------------------------------------------------------------------------------------------------------
	  while (Cal_Phase == 11)
	  {
		  if (Position >= (UpperEnd1 -2))
		  {
			  MC_StopMotor1();
			  Cal_Phase = 12;
			  Torque = 0;
		  }
		  HAL_Delay(0);
	  }

	  // FLASH PROGRAM -------------------------------------------------------------------------------------------------------------------
	  while (Cal_Phase == 12)
	  {
		  if (IsFlashWritten == 0)
		  {
			  HAL_FLASH_Unlock();
			  HAL_FLASH_OB_Unlock();

			  FLASH_EraseInitTypeDef EraseInitStruct;
			  EraseInitStruct.TypeErase = FLASH_TYPEERASE_PAGES;
			  EraseInitStruct.Page =  30;
			  EraseInitStruct.NbPages = 2;
			  uint32_t PageError;

			  HAL_FLASHEx_Erase(&EraseInitStruct, &PageError);
			  HAL_Delay(1000);
			  ProgramADDR = 0x0800F000;
			  while (iteration <= 858)
			  {

				  checekbuffer[0] = ProgramBuffer[iteration] >> 8;
				  checekbuffer[1] = ProgramBuffer[iteration];
				  checekbuffer[2] = ProgramBuffer[iteration + 1] >> 8;
				  checekbuffer[3] = ProgramBuffer[iteration + 1];

				  if (checekbuffer[0] != 0)
				  {
					  if (checekbuffer[1] == 0 && checekbuffer[2] == 0 && checekbuffer[3] == 0)
					  {
						  ProgramBuffer[iteration+1] = 0xFFFFFFFF;
						  ProgramBuffer[iteration] |= 0xFFFF0000;
					  }
					  if (checekbuffer[2] == 0 && checekbuffer[3] == 0)
					  {
						  ProgramBuffer[iteration+1] = 0xFFFFFFFF;
					  }
					  if (checekbuffer[3] == 0)
					  {
						  ProgramBuffer[iteration] |= 0xFFFF0000;
					  }
				  }

				  Program = ProgramBuffer[iteration+1];
				  Program = Program << 32;
				  Program = Program + ProgramBuffer[iteration];

				  if (Program != 0)
				  {
					  HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, ProgramADDR, Program);
					  HAL_Delay(5);
					  ProgramADDR += 8;
				  }
				  iteration = iteration +2;
			  }


		  HAL_Delay(100);

		  HAL_FLASH_OB_Lock();
		  HAL_FLASH_Lock();
		  IsFlashWritten = 1;
		  Cal_Phase = 13;
		  }
	  }
	  // GO BACK TO 0 POSITION-------------------------------------------------------------
	  while (Cal_Phase == 13)
	  {
		  MC_ProgramTorqueRampMotor1(Torque, 10);
		  MC_StartMotor1();

		  for (int i = 0; i < 4; i++ )
		  {
			  Speed[i] = MC_GetMecSpeedAverageMotor1();
			  HAL_Delay(2);
		  }

		  if (Speed[0] >= 60 && Speed[1] >= 60 && Speed[2] >= 60 && Speed[3] >= 60 )
			  {
				  Cal_Phase = 14;
			  }

		  if (Speed[0] <= 60 && Speed[1] <= 60 && Speed[2] <= 60 && Speed[3] <= 60 )
		  {
			  Torque = Torque - 25;
			  MC_ProgramTorqueRampMotor1(Torque, 10);
			  HAL_Delay(50);
		  }

	  }

	  // ZERO POSITION REACHED ---------------------------------------------------------------------------------------------------------------------
	  while(Cal_Phase == 14)
	  {
		  if (Position <= 0)
		  {
			  MC_StopMotor1();
			  Cal_Phase = 100;
			  Torque = 0;
		  }
		  HAL_Delay(0);
	  }

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Configure the main internal regulator output voltage
  */
  HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1_BOOST);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV4;
  RCC_OscInitStruct.PLL.PLLN = 80;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV8;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the peripherals clocks
  */
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_ADC12;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.Adc12ClockSelection = RCC_ADC12CLKSOURCE_PLL;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief NVIC Configuration.
  * @retval None
  */
static void MX_NVIC_Init(void)
{
  /* TIM1_BRK_TIM15_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM1_BRK_TIM15_IRQn, 4, 1);
  HAL_NVIC_EnableIRQ(TIM1_BRK_TIM15_IRQn);
  /* TIM1_UP_TIM16_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM1_UP_TIM16_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(TIM1_UP_TIM16_IRQn);
  /* ADC1_2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(ADC1_2_IRQn, 2, 0);
  HAL_NVIC_EnableIRQ(ADC1_2_IRQn);
  /* TIM2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM2_IRQn, 3, 0);
  HAL_NVIC_EnableIRQ(TIM2_IRQn);
  /* USART2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(USART2_IRQn, 3, 1);
  HAL_NVIC_EnableIRQ(USART2_IRQn);
  /* EXTI15_10_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 3, 0);
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_MultiModeTypeDef multimode = {0};
  ADC_InjectionConfTypeDef sConfigInjected = {0};
  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Common config
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV4;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.DataAlign = ADC_DATAALIGN_LEFT;
  hadc1.Init.GainCompensation = 0;
  hadc1.Init.ScanConvMode = ADC_SCAN_ENABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc1.Init.LowPowerAutoWait = DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  hadc1.Init.OversamplingMode = DISABLE;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the ADC multi-mode
  */
  multimode.Mode = ADC_MODE_INDEPENDENT;
  if (HAL_ADCEx_MultiModeConfigChannel(&hadc1, &multimode) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Injected Channel
  */
  sConfigInjected.InjectedChannel = ADC_CHANNEL_2;
  sConfigInjected.InjectedRank = ADC_INJECTED_RANK_1;
  sConfigInjected.InjectedSamplingTime = ADC_SAMPLETIME_6CYCLES_5;
  sConfigInjected.InjectedSingleDiff = ADC_SINGLE_ENDED;
  sConfigInjected.InjectedOffsetNumber = ADC_OFFSET_NONE;
  sConfigInjected.InjectedOffset = 0;
  sConfigInjected.InjectedNbrOfConversion = 2;
  sConfigInjected.InjectedDiscontinuousConvMode = DISABLE;
  sConfigInjected.AutoInjectedConv = DISABLE;
  sConfigInjected.QueueInjectedContext = DISABLE;
  sConfigInjected.ExternalTrigInjecConv = ADC_EXTERNALTRIGINJEC_T1_TRGO;
  sConfigInjected.ExternalTrigInjecConvEdge = ADC_EXTERNALTRIGINJECCONV_EDGE_RISING;
  sConfigInjected.InjecOversamplingMode = DISABLE;
  if (HAL_ADCEx_InjectedConfigChannel(&hadc1, &sConfigInjected) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Injected Channel
  */
  sConfigInjected.InjectedChannel = ADC_CHANNEL_14;
  sConfigInjected.InjectedRank = ADC_INJECTED_RANK_2;
  if (HAL_ADCEx_InjectedConfigChannel(&hadc1, &sConfigInjected) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_1;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_47CYCLES_5;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief ADC2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC2_Init(void)
{

  /* USER CODE BEGIN ADC2_Init 0 */

  /* USER CODE END ADC2_Init 0 */

  ADC_InjectionConfTypeDef sConfigInjected = {0};
  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC2_Init 1 */

  /* USER CODE END ADC2_Init 1 */
  /** Common config
  */
  hadc2.Instance = ADC2;
  hadc2.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV4;
  hadc2.Init.Resolution = ADC_RESOLUTION_12B;
  hadc2.Init.DataAlign = ADC_DATAALIGN_LEFT;
  hadc2.Init.GainCompensation = 0;
  hadc2.Init.ScanConvMode = ADC_SCAN_ENABLE;
  hadc2.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc2.Init.LowPowerAutoWait = DISABLE;
  hadc2.Init.ContinuousConvMode = DISABLE;
  hadc2.Init.NbrOfConversion = 1;
  hadc2.Init.DiscontinuousConvMode = DISABLE;
  hadc2.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc2.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc2.Init.DMAContinuousRequests = DISABLE;
  hadc2.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  hadc2.Init.OversamplingMode = DISABLE;
  if (HAL_ADC_Init(&hadc2) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Injected Channel
  */
  sConfigInjected.InjectedChannel = ADC_CHANNEL_14;
  sConfigInjected.InjectedRank = ADC_INJECTED_RANK_1;
  sConfigInjected.InjectedSamplingTime = ADC_SAMPLETIME_6CYCLES_5;
  sConfigInjected.InjectedSingleDiff = ADC_SINGLE_ENDED;
  sConfigInjected.InjectedOffsetNumber = ADC_OFFSET_NONE;
  sConfigInjected.InjectedOffset = 0;
  sConfigInjected.InjectedNbrOfConversion = 2;
  sConfigInjected.InjectedDiscontinuousConvMode = DISABLE;
  sConfigInjected.AutoInjectedConv = DISABLE;
  sConfigInjected.QueueInjectedContext = DISABLE;
  sConfigInjected.ExternalTrigInjecConv = ADC_EXTERNALTRIGINJEC_T1_TRGO;
  sConfigInjected.ExternalTrigInjecConvEdge = ADC_EXTERNALTRIGINJECCONV_EDGE_RISING;
  sConfigInjected.InjecOversamplingMode = DISABLE;
  if (HAL_ADCEx_InjectedConfigChannel(&hadc2, &sConfigInjected) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Injected Channel
  */
  sConfigInjected.InjectedChannel = ADC_CHANNEL_3;
  sConfigInjected.InjectedRank = ADC_INJECTED_RANK_2;
  if (HAL_ADCEx_InjectedConfigChannel(&hadc2, &sConfigInjected) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_3;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_47CYCLES_5;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  if (HAL_ADC_ConfigChannel(&hadc2, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC2_Init 2 */

  /* USER CODE END ADC2_Init 2 */

}

/**
  * @brief CORDIC Initialization Function
  * @param None
  * @retval None
  */
static void MX_CORDIC_Init(void)
{

  /* USER CODE BEGIN CORDIC_Init 0 */

  /* USER CODE END CORDIC_Init 0 */

  /* USER CODE BEGIN CORDIC_Init 1 */

  /* USER CODE END CORDIC_Init 1 */
  hcordic.Instance = CORDIC;
  if (HAL_CORDIC_Init(&hcordic) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN CORDIC_Init 2 */

  /* USER CODE END CORDIC_Init 2 */

}

/**
  * @brief SPI2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI2_Init(void)
{

  /* USER CODE BEGIN SPI2_Init 0 */

  /* USER CODE END SPI2_Init 0 */

  /* USER CODE BEGIN SPI2_Init 1 */

  /* USER CODE END SPI2_Init 1 */
  /* SPI2 parameter configuration*/
  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_14BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi2.Init.NSS = SPI_NSS_SOFT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_16;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 7;
  hspi2.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi2.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI2_Init 2 */

  /* USER CODE END SPI2_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_SlaveConfigTypeDef sSlaveConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = ((TIM_CLOCK_DIVIDER) - 1);
  htim1.Init.CounterMode = TIM_COUNTERMODE_CENTERALIGNED1;
  htim1.Init.Period = ((PWM_PERIOD_CYCLES) / 2);
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV2;
  htim1.Init.RepetitionCounter = (REP_COUNTER);
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sSlaveConfig.SlaveMode = TIM_SLAVEMODE_TRIGGER;
  sSlaveConfig.InputTrigger = TIM_TS_ITR1;
  if (HAL_TIM_SlaveConfigSynchro(&htim1, &sSlaveConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_OC4REF;
  sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_SET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.Pulse = 0;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.Pulse = 0;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM2;
  sConfigOC.Pulse = 0;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_ENABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_ENABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_1;
  sBreakDeadTimeConfig.DeadTime = ((DEAD_TIME_COUNTS) / 2);
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_LOW;
  sBreakDeadTimeConfig.BreakFilter = 0;
  sBreakDeadTimeConfig.BreakAFMode = TIM_BREAK_AFMODE_INPUT;
  sBreakDeadTimeConfig.Break2State = TIM_BREAK2_DISABLE;
  sBreakDeadTimeConfig.Break2Polarity = TIM_BREAK2POLARITY_HIGH;
  sBreakDeadTimeConfig.Break2Filter = 0;
  sBreakDeadTimeConfig.Break2AFMode = TIM_BREAK_AFMODE_INPUT;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */
  HAL_TIM_MspPostInit(&htim1);

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_HallSensor_InitTypeDef sConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 0;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = M1_HALL_TIM_PERIOD;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfig.IC1Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC1Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC1Filter = M1_HALL_IC_FILTER;
  sConfig.Commutation_Delay = 0;
  if (HAL_TIMEx_HallSensor_Init(&htim2, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_OC2REF;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief TIM6 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM6_Init(void)
{

  /* USER CODE BEGIN TIM6_Init 0 */

  /* USER CODE END TIM6_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM6_Init 1 */

  /* USER CODE END TIM6_Init 1 */
  htim6.Instance = TIM6;
  htim6.Init.Prescaler = 0;
  htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim6.Init.Period = 15999;
  htim6.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim6, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM6_Init 2 */

  /* USER CODE END TIM6_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.Init.ClockPrescaler = UART_PRESCALER_DIV1;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetTxFifoThreshold(&huart2, UART_TXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetRxFifoThreshold(&huart2, UART_RXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_DisableFifoMode(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMAMUX1_CLK_ENABLE();
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(MA730_CS_GPIO_Port, MA730_CS_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : Start_Stop_Pin */
  GPIO_InitStruct.Pin = Start_Stop_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(Start_Stop_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PA5 */
  GPIO_InitStruct.Pin = GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : MA730_CS_Pin */
  GPIO_InitStruct.Pin = MA730_CS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(MA730_CS_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

void GetRotorPosition(void)
{

	// GET POSITION FROM HALL SENSORS --------------------------------------------------------------------------
	bPrevHallStatus=HallStatus2;
	HallStatus2 = HallStatus;

	switch ( HallStatus2 )
	{
		case PHASE_1:
			if ( bPrevHallStatus == PHASE_6 )
		 	{
				Position = Position +1;
		 	}
		 	else if ( bPrevHallStatus == PHASE_2 )
		 	{
		 		Position = Position -1;
		 	}
		 	else
		 	{
		 	}
		break;

		case PHASE_2:
			if ( bPrevHallStatus == PHASE_1 )
			{
				Position = Position +1;
			}
			else if ( bPrevHallStatus == PHASE_3 )
			{
				Position = Position -1;
			}
			else
			{
			}
		break;

		case PHASE_3:
			if ( bPrevHallStatus == PHASE_2 )
			{
				Position = Position +1;
			}
			else if ( bPrevHallStatus == PHASE_4 )
			{
				Position = Position -1;
			}
			else
			{
			}
		break;

		case PHASE_4:
			if ( bPrevHallStatus == PHASE_3 )
			{
				Position = Position +1;
			}
			else if ( bPrevHallStatus == PHASE_5 )
			{
				Position = Position -1;
			}
			else
			{
			}
		break;

		case PHASE_5:
			if ( bPrevHallStatus == PHASE_4 )
			{
				Position = Position +1;
			}
			else if ( bPrevHallStatus == PHASE_6 )
			{
				Position = Position -1;
			}
			else
			{
			}
		break;

		case PHASE_6:
			if ( bPrevHallStatus == PHASE_5 )
			{
				Position = Position +1;

			}
			else if ( bPrevHallStatus == PHASE_1 )
			{
				Position = Position -1;
			}
			else
			{
			}
		break;
	}

	// GET POSITION FROM MA730 ----------------------------------------------------------------------------------
	if ( (Position % 6) == 0 && (Cal_Phase == 11 || Cal_Phase == 10) && LastPosition != Position)
	{
		LastPosition = Position;
		HAL_GPIO_WritePin(MA730_CS_GPIO_Port, MA730_CS_Pin, GPIO_PIN_RESET);
		HAL_SPI_Receive_IT(&hspi2, &SPI_Buffer2, 14);
	//	HAL_GPIO_WritePin(MA730_CS_GPIO_Port, MA730_CS_Pin, GPIO_PIN_SET);
	}
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim6)
{
//	if(htim->Instance == TIM6)
//	{
		GetRotorPosition();

//	}
}
void HAL_SPI_RxCpltCallback(SPI_HandleTypeDef * hspi2)
{
	HAL_GPIO_WritePin(MA730_CS_GPIO_Port, MA730_CS_Pin, GPIO_PIN_SET);
	//SPI_Buffer2 = SPI_Buffer2 >> 2;
	SPI_Buffer = SPI_Buffer2;
	SPI_Buffer2 = 0;

	// Write data to buffer 0 ---------------------------------------------------------------
	if (ProgramBuffer[ProgramID] != 0)
	{
		ProgramBuffer[ProgramID] = ProgramBuffer[ProgramID] + (SPI_Buffer << 16);
		ProgramID ++;
	}
	else
	{
		ProgramBuffer[ProgramID] = SPI_Buffer;
	}

}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
